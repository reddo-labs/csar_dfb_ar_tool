#include "PhototoolStateCreator.h"
namespace reddo {
	namespace phototool {
		shared_ptr<ToolStateConfig> PhototoolStateCreator::getToolStateConfig(pair<string, StateMapping> flow, StateCreatorSettings settings, ofxInterface::Node * stateNode, ofJson stateDesc)
		{
			shared_ptr < PhotoToolStateConfig> config = shared_ptr < PhotoToolStateConfig>(new PhotoToolStateConfig());
			config->name = flow.first;
			config->firstState = flow.second.firstState;
			config->nextState = flow.second.nextState;
			config->previousState = flow.second.previousState;
			config->node = stateNode;
			config->stateDesc = stateDesc;
			config->toolSettings = settings.toolSettings;
			config->canvasControl = static_pointer_cast<PhotoToolParams>(settings.toolParams)->canvasControl;
			config->cameraControl = static_pointer_cast<PhotoToolParams>(settings.toolParams)->cameraControl;
			config->exportControl = static_pointer_cast<PhotoToolParams>(settings.toolParams)->exportControl;
			return config;
		}

		// creates the state flow
		shared_ptr<ToolState> PhototoolStateCreator::createState(shared_ptr < ToolStateConfig> c)
		{
			shared_ptr<PhotoToolState> state;
			shared_ptr < PhotoToolStateConfig> config = static_pointer_cast<PhotoToolStateConfig>(c);

			if (config->name == "idle") {
				state = shared_ptr<PhotoToolState>(new IdleState());
				state->setup(*config);
			}
			else if (config->name == "selection") {
				state = shared_ptr<PhotoToolState>(new SelectionState());
				state->setup(*config);
			}
			else if (config->name == "selectPosition") {
				state = shared_ptr<PhotoToolState>(new SelectPositionState());
				state->setup(*config);
			}
			else if (config->name == "countdown") {
				state = shared_ptr<PhotoToolState>(new CountdownState());
				state->setup(*config);
			}
			else if (config->name == "captureVideo") {
				state = shared_ptr<PhotoToolState>(new CaptureVideoState());
				state->setup(*config);
			}
			else if (config->name == "selectHead") {
				state = shared_ptr<PhotoToolState>(new SelectHeadState());
				state->setup(*config);
			}
			else if (config->name == "checkPhoto") {
				state = shared_ptr<PhotoToolState>(new CheckPhotoState());
				state->setup(*config);
			}
			else if (config->name == "pictureSelect") {
				state = shared_ptr<PhotoToolState>(new PictureSelectState());
				state->setup(*config);
			}
			else if (config->name == "icons") {
				state = shared_ptr<PhotoToolState>(new IconState());
				state->setup(*config);
			}
			else if (config->name == "lead") {
				state = shared_ptr<PhotoToolState>(new SamsungLeadState());
				state->setup(*config);
			}
			else if (config->name == "end") {
				state = shared_ptr<PhotoToolState>(new EndState());
				state->setup(*config);
			}
			else if (config->name == "win") {
				state = shared_ptr<PhotoToolState>(new WinState());
				state->setup(*config);
			}
			else if (config->name == "setup") {
				state = shared_ptr<PhotoToolState>(new SetupState());
				state->setup(*config);
			}
			return state;
		}
		map<string, StateMapping> PhototoolStateCreator::createStateMapping(ofJson settings)
		{
			auto stateConfigs = settings["config"];

			// the complete flow
			//vector<string> stateFlow = { "idle","selection","countdown","checkPhoto","pictureSelect","lead","icons","end" };
			//vector<string> stateFlow = { "idle","selection","selectPosition","countdown","selectHead","checkPhoto","pictureSelect","lead","icons","print","end" };
			vector<string> stateFlow = { "idle","selection","selectPosition","lead","countdown","selectHead","checkPhoto","pictureSelect","icons","end","win" };

			// remove all unused states
			if (stateConfigs["pictureSelect"]["active"] == false) { // if picture select is active, remove photo function
				stateFlow.erase(stateFlow.begin() + ofFind(stateFlow, string("pictureSelect")));

				// select state
				int nPreviews = GPF::listFiles(settings["animatedOverlay"]["videosPreview"]).size();
				int nOverlays = GPF::listFiles(settings["animatedOverlay"]["videosOverlay"]).size();
				int nBackgrounds = GPF::listFiles(stateConfigs["animatedOverlay"]["videosBackground"]).size();


				if (stateConfigs["icons"]["active"] == false) {
					stateFlow.erase(stateFlow.begin() + ofFind(stateFlow, string("icons")));
				}
				if (stateConfigs["lead"]["dataProtection"] == false
					&& stateConfigs["lead"]["furtherUse"] == false) {
					stateFlow.erase(stateFlow.begin() + ofFind(stateFlow, string("lead")));
				}
			}
			else {
				stateFlow.erase(stateFlow.begin() + ofFind(stateFlow, string("idle")));
				stateFlow.erase(stateFlow.begin() + ofFind(stateFlow, string("countdown")));
				stateFlow.erase(stateFlow.begin() + ofFind(stateFlow, string("captureVideo")));
				stateFlow.erase(stateFlow.begin() + ofFind(stateFlow, string("checkPhoto")));
				stateFlow.erase(stateFlow.begin() + ofFind(stateFlow, string("selection")));
				stateFlow.erase(stateFlow.begin() + ofFind(stateFlow, string("lead")));
			}

			map<string, StateMapping> ret;
			for (size_t i = 0; i < stateFlow.size(); i++) {
				StateMapping s;
				s.firstState = stateFlow.front();
				if (i < stateFlow.size() - 1) s.nextState = stateFlow[i + 1];
				else s.nextState = stateFlow[0];
				if (i > 0)s.previousState = stateFlow[i - 1];
				ret.insert(pair<string, StateMapping>(stateFlow[i], s));
			}

			// add non configurable states
			StateMapping stateSetup;
			stateSetup.firstState = stateFlow.front();
			stateSetup.nextState = "idle";
			stateSetup.previousState = "idle";
			ret.insert(pair<string, StateMapping>("setup", stateSetup));

			return ret;
		}
	}
}
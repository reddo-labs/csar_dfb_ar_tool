#include "Phototool.h"
#include "GPF.h"

using namespace ofxModule;

namespace reddo {
	namespace phototool {

		Phototool::Phototool(string moduleName, string settingsPath):ToolControl( moduleName, settingsPath) {
			setup();

			// activate idle state if PhotoMode otherwise activate pictureSelect
			if (params->states.find("idle") != params->states.end()) {
				currentState = params->states["idle"];
			}
			else {
				currentState = params->states["pictureSelect"];
			}
			currentState->activate();
		}

		//------------------------------------------------------------------
		void Phototool::update() {
			ToolControl::update();

			// check timeout
			if (currentState->getName() != "idle" && currentState->getName() != "countdown" &&
				ofGetElapsedTimeMillis() - lastAction > settings["general"]["noActionTimeout"].get<int>()) {
				setState("idle");
			}

		}

		///\brief redirects module events from internal Modules to global module control
		void Phototool::onToolModuleEvent(ModuleEvent & e) {
			if (e.address == "exportImage" || e.address == "capture" || e.address == "exportMovie" || e.address == "copy") {
				proceedImageExport(e);
			}else if (e.address == "currentImageId") {
				if (params->states.find("icons") != params->states.end()) {
					params->states["icons"]->onModuleEvent(e);
				}
			}
			else if(params->states.find("pictureSelect") != params->states.end()){
				if (e.address == "deleteImageFromGallery") {
					static_pointer_cast<PictureSelectState>(params->states["pictureSelect"])->deleteLastSelectedItem();
				}
				else if (e.address == "imagePrinted") {
					static_pointer_cast<PictureSelectState>(params->states["pictureSelect"])->printLastSelectedItem();
				}
			}
			else if (e.address == "exportMode") {
				params->states["end"]->onModuleEvent(e);
			}

			// LED events
			else if (ofIsStringInString( e.address,"led_") ){
				notifyEvent(e.address);
			}
		}

		///\brief next state when capture event notified
		void Phototool::onCaptured() {
			ModuleEvent e = ModuleEvent("Phototool", "Phototool", "captured", ofJson());
			currentState->onModuleEvent(e);
			//setState(currentState->getNextState());
		}

		void Phototool::initParams()
		{
			params = shared_ptr<PhotoToolParams>(new PhotoToolParams());
		}

		void Phototool::initGuiFactory()
		{
			guiFactory = shared_ptr<ofxInterface::arBoothGui::CsarDfbGuiFactory>(new ofxInterface::arBoothGui::CsarDfbGuiFactory());
		}

		///\brief redirects global events to internal controllers
		void Phototool::proceedModuleEvent(ModuleEvent& e) {

			if (e.id == "Capture" || e.moduleClass == "Greenscreen") {
				static_pointer_cast<PhotoToolParams>(params)->cameraControl->onModuleEvent(e);
			}
		}

		void Phototool::proceedImageExport(ModuleEvent & e) {
			notifyEvent(e);
		}

		void Phototool::setupCanvas(ofJson guiDef) {

			// init canvas, fit rectangle to format
			ofRectangle rMax(0, 0, settings["picture"]["dimCanvas"][0].get<int>(), settings["picture"]["dimCanvas"][1].get<int>());
			ofRectangle rRef(0, 0, settings["picture"]["pictureFormat"].get<float>() * 100.0f, 100.0f);

			rRef.scaleTo(rMax, OF_SCALEMODE_FIT);

			CanvasSettings canvasSettings;
			canvasSettings.width = round(rRef.width);
			canvasSettings.height = round(rRef.height);
			if (settings["picture"]["mode"] == "movie") {
				canvasSettings.isVideo = true;
			}else {
				canvasSettings.isVideo = false;
			}

			params->canvas = CanvasControl::createCanvas(canvasSettings);

			// create additional layers
			//createElementNode("overlay", params->canvas);
			//createElementNode("background", params->canvas);
			loadAssetsInCanvas(params->canvas);

		}

		void Phototool::setupControllers() {


			// init camera control
			CameraControlSettings ccSettings;
			ccSettings.camNode = static_cast<ofxInterface::TextureNode*>(params->canvas->getChildWithName("photo")->getChildWithName("node_photo"));
			ccSettings.image = static_cast<ofxInterface::TextureNode*>(params->canvas->getChildWithName("photo")->getChildWithName("photo0"));
			ccSettings.config = settings;


			auto paramsT = static_pointer_cast<PhotoToolParams>(params);

			paramsT->cameraControl = shared_ptr<CameraControl>(new CameraControl());
			paramsT->cameraControl->setup(ccSettings);
			registerStateEvent(paramsT->cameraControl->moduleEvent);
			ofAddListener(paramsT->cameraControl->captureFinishedEvent, this, &Phototool::onCaptured);

			// init canvas control
			CanvasControlSettings canvascSettings;
			canvascSettings.canvas = static_cast<ofxInterface::PhototoolCanvas*>(params->canvas);
			canvascSettings.nBackgrounds = settings["config"]["backgrounds"].size();
			canvascSettings.nOverlays = settings["config"]["overlays"].size();
			canvascSettings.loopType = settings["picture"]["loopMode"].get<string>() == "palindrome" ? OF_LOOP_PALINDROME : OF_LOOP_NORMAL;
			canvascSettings.isVideoRecord = settings["picture"]["mode"].get<string>() == "movie" ? true : false ;
			paramsT->canvasControl = shared_ptr<CanvasControl>(new CanvasControl());
			paramsT->canvasControl->setup(canvascSettings);

			
		}

		void Phototool::initStateCreator()
		{
			stateCreator = shared_ptr<PhototoolStateCreator>(new PhototoolStateCreator());
		}

		

		///\brief creates Canvas nodes including multiple subnodes (overlay, background)
		void Phototool::createElementNode(string elemName, ofxInterface::Canvas * canvas) {
	
			// create nodes 
			ofxInterface::reddoGui::HapPlayerNode* tBase = static_cast<ofxInterface::reddoGui::HapPlayerNode*>(canvas->getChildWithName(elemName)->getChildWithName("node_" + elemName));
			tBase->setSize(canvas->getSize());
			for (size_t i = 0; i < settings["animatedOverlay"]["nVideosPlay"].get<int>(); i++) {
				ofxInterface::reddoGui::HapPlayerNode* t = new ofxInterface::reddoGui::HapPlayerNode();
				t->setLoopState(ofLoopType::OF_LOOP_NONE);
				t->setName("node_" + ofToString(i));
				t->setSize(canvas->getWidth(), canvas->getHeight());
				canvas->getChildWithName(elemName)->addChild(t);

				//tBase->setTexture(params->assets->getTexture(elemName + "0"));
			}
		}

		void Phototool::loadAssetsInCanvas(ofxInterface::Canvas * canvas)
		{
			// load all images , movies are loaded dynamically

			

			// yogi

			// bg
			ofxInterface::reddoGui::HapPlayerNode* y1 = new ofxInterface::reddoGui::HapPlayerNode();
			y1->setLoopState(ofLoopType::OF_LOOP_NONE);
			y1->setName("yogi_bg");
			y1->setSize(canvas->getWidth(), canvas->getHeight());
			y1->setTexture(GPF::loadTexture(settings["scenes"]["yogi"]["bg"].get<string>()));
			y1->setPlane(0);
			canvas->getChildWithName("background")->addChild(y1);

			// overlay
			/*ofxInterface::reddoGui::HapPlayerNode* y2 = new ofxInterface::reddoGui::HapPlayerNode();
			y2->setLoopState(ofLoopType::OF_LOOP_NONE);
			y2->setName("yogi_overlay");
			y2->setSize(canvas->getWidth(), canvas->getHeight());
			y2->load(settings["scenes"]["yogi"]["bg"].get<string>());
			canvas->getChildWithName("overlay")->addChild(y2);*/

			// selfie

			// bg
			ofxInterface::reddoGui::HapPlayerNode* s1 = new ofxInterface::reddoGui::HapPlayerNode();
			s1->setLoopState(ofLoopType::OF_LOOP_NONE);
			s1->setName("selfie_bg");
			s1->setPlane(0);
			s1->setSize(canvas->getWidth(), canvas->getHeight());
			s1->setTexture(GPF::loadTexture(settings["scenes"]["selfie"]["bg"].get<string>()));
			canvas->getChildWithName("background")->addChild(s1);
			
			
			auto nNode = canvas->getChildWithName("background")->getChildWithName("node_background");
			canvas->getChildWithName("background")->removeChild(nNode);

			// hack : recreate bg video node to hae it over bg image node
			ofxInterface::reddoGui::HapPlayerNode* newBgNode = new ofxInterface::reddoGui::HapPlayerNode();
			newBgNode->setName("node_background");
			newBgNode->setSize(canvas->getWidth(), canvas->getHeight());
			canvas->getChildWithName("background")->addChild(newBgNode);

			// team

			// bg
			ofxInterface::reddoGui::HapPlayerNode* t1 = new ofxInterface::reddoGui::HapPlayerNode();
			t1->setLoopState(ofLoopType::OF_LOOP_NONE);
			t1->setName("team_bg");
			t1->setPlane(0);
			t1->setSize(canvas->getWidth(), canvas->getHeight());
			t1->setTexture(GPF::loadTexture(settings["scenes"]["team"]["bg"].get<string>()));
			canvas->getChildWithName("background")->addChild(t1);

			// overlay - player

			ofxInterface::Node* player = new ofxInterface::Node();
			player->setName("player");
			canvas->getChildWithName("overlay")->addChild(player);

			for (auto& player :settings["scenes"]["team"]["positions"])
			{
				ofxInterface::reddoGui::HapPlayerNode* t2 = new ofxInterface::reddoGui::HapPlayerNode();
				t2->setLoopState(ofLoopType::OF_LOOP_NONE);
				t2->setName(player["description"].get<string>());
				t2->setSize(canvas->getWidth(), canvas->getHeight());
				t2->setTexture(GPF::loadTexture(player["file"].get<string>()));
				canvas->getChildWithName("overlay")->getChildWithName("player")->addChild(t2);
			}

			// overlay stadium
			ofxInterface::reddoGui::HapPlayerNode* t3 = new ofxInterface::reddoGui::HapPlayerNode();
			t3->setLoopState(ofLoopType::OF_LOOP_NONE);
			t3->setName("team_overlay");
			t3->setSize(canvas->getWidth(), canvas->getHeight());
			t3->setTexture(GPF::loadTexture(settings["scenes"]["team"]["overlayPic"].get<string>()));
			canvas->getChildWithName("overlay")->addChild(t3);



			// overlay
			vector<string> oNodes = { "selfie","team","yogi" };
			for (auto& v:oNodes)
			{
				ofxInterface::reddoGui::HapPlayerNode* t = new ofxInterface::reddoGui::HapPlayerNode();
				t->setLoopState(ofLoopType::OF_LOOP_NONE);
				t->setName("node_logo_" + v);
				t->setSize(canvas->getWidth(), canvas->getHeight());
				if (settings["scenes"][v]["overlay"].get<string>() != "") {
					t->setTexture(GPF::loadTexture(settings["scenes"][v]["overlay"].get<string>()));
				}
				t->setPlane(100);
				t->deactivate();
				canvas->getChildWithName("overlay")->addChild(t);
			}
		}

	}
}
#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	// killswitch eos utility

	ofSystem("Taskkill /IM \"EOS Utility.exe\" /F");
	ofSystem("Taskkill /IM \"EOS Utility 3.exe\" /F");

	ofSleepMillis(900);

	ofSetFrameRate(30);
	ofSetCircleResolution(200);
	ofSetCurveResolution(200);
	ofLogToConsole();

	

	string settingsPath = reddo::ToolControl::checkAndGetConfigPath(PRODUCT, VERSION);

	if (webModule.getModuleLoader()->getCountModulesConfig("Webcam")) {
		webModule.getModuleLoader()->addModule(new ofxModuleWebcam("Capture",settingsPath));
	}
	if (webModule.getModuleLoader()->getCountModulesConfig("Canon")) {
		webModule.getModuleLoader()->addModule(new ofxModuleCanon("Capture", settingsPath));
	}
	if (webModule.getModuleLoader()->getCountModulesConfig("Phototool")) {
		webModule.getModuleLoader()->addModule(new reddo::phototool::Phototool("Phototool", settingsPath));
	}
	if (webModule.getModuleLoader()->getCountModulesConfig("Greenscreen")) {
		webModule.getModuleLoader()->addModule(new ofxModuleGreenscreen("Greenscreen", settingsPath));
	}
	if (webModule.getModuleLoader()->getCountModulesConfig("PictureExporter")) {
		webModule.getModuleLoader()->addModule(new ofxModulePictureExporter("PictureExporter", settingsPath));
	}
	if (webModule.getModuleLoader()->getCountModulesConfig("PhototoolLighting")) {
		webModule.getModuleLoader()->addModule(new ofxReddoPhototoolLighting("PhototoolLighting",settingsPath));
	}

	webModule.getModuleLoader()->initModuleCommunication();
}

//--------------------------------------------------------------
void ofApp::update(){
	webModule.getModuleRunner()->updateModules();
}

//--------------------------------------------------------------
void ofApp::draw(){
	webModule.getModuleRunner()->drawModules();
}


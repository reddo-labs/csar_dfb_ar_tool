#ifndef _Phototool
#define _Phototool


#include "ToolControl.h"
#include "TextureNode.h"
#include "HapPlayerNode.h"
#include "Coverflow.h"

#include "PhotoToolParams.h"
#include "CsarDfbGuiFactory.h"
#include "PhotoToolState.h"
#include "MoviePreviewLoadJob.h"
#include "PhototoolStateCreator.h"

using namespace ofxModule;

namespace reddo {
	namespace phototool {

		///\brief phototool main control
		///
		/// controls state generation, state flow and module communication and rendering
		class Phototool : public ToolControl {

		public:

			Phototool(string moduleName = "Phototool", string settingsPath = "settings.json");

			void update() override;
			void onToolModuleEvent(ModuleEvent& e) override;
			void onCaptured();

		protected:
			void initParams() override;
			void initGuiFactory() override;

			void proceedModuleEvent(ModuleEvent& e);
			void proceedImageExport(ModuleEvent& e);

			void setupCanvas(ofJson guiDef) override;
			void setupControllers() override;
			void initStateCreator() override;
			void createElementNode(string elemName, ofxInterface::Canvas* canvas);
			void loadAssetsInCanvas(ofxInterface::Canvas* canvas);

		private:			
		};
	}
}

#endif

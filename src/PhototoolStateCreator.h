#pragma once
#include "StateCreator.h"
#include "PhotoToolState.h"
#include "PhotoToolParams.h"

#include "IdleState.h"
#include "SelectPositionState.h"
#include "SelectHeadState.h"
#include "CountdownState.h"
#include "CaptureVideoState.h"
#include "CheckPhotoState.h"
#include "SelectionState.h"
#include "SetupState.h"
#include "SamsungLeadState.h"
#include "IconState.h"
#include "PictureSelectState.h"
#include "EndState.h"
#include "WinState.h"

namespace reddo {
	namespace phototool {
		class PhototoolStateCreator : public StateCreator
		{
			shared_ptr <ToolStateConfig> getToolStateConfig(pair<string, StateMapping> flow, StateCreatorSettings settings, ofxInterface::Node* stateNode, ofJson stateDesc) override;
			shared_ptr<ToolState> createState(shared_ptr < ToolStateConfig> c) override;
			map<string, StateMapping> createStateMapping(ofJson settings) override;
		};
	}
}



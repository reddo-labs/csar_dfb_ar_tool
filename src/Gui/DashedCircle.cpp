#include "DashedCircle.h"

namespace ofxInterface {
	namespace arBoothGui {



		void DashedCircle::setup(DashedCircleSettings settings)
		{

			Node::setup(settings);
			widthCircle = settings.widthCircle;
			color = settings.color;
			dashAngleStroke = settings.dashAngleStroke;
			dashAngleEmpty = settings.dashAngleEmpty;

			// ring
			initRing();

			
			ofAddListener(eventNodeSizeChanged, this, &DashedCircle::onSizeChanged);
		}


		void DashedCircle::draw()
		{
			// draw empty ring
			ring.draw();



		}

		void DashedCircle::onSizeChanged(Node& n)
		{
			initRing();
		}

		void DashedCircle::initRing()
		{
			ring.clear();

			ofVec2f center = ofVec2f(getWidth(), getHeight())*0.5;
			float radius = min(getWidth(), getHeight())*0.5;
			float circ = PI * radius;
			float degPerPx = 360 / circ;

			float angle = 0;
			ring.setArcResolution(200);
			ring.moveTo(radius, radius);

			while (angle < 360)
			{
				ring.arc(center, radius, radius, angle, angle + dashAngleStroke);
				ring.arcNegative(center, radius - widthCircle, radius - widthCircle, angle + dashAngleStroke, angle);
				angle += (dashAngleEmpty + dashAngleStroke);
				angle = ofClamp(angle, 0, 360);
				ring.moveTo(radius, radius);
			}

			ring.close();
			ring.setColor(color);

			//ring.scale(0.5, 0.5);
		}


	


}
}

#include "CsarDfbGuiFactory.h"

namespace ofxInterface {
	namespace arBoothGui {
		void CsarDfbGuiFactory::setup(ofJson config, shared_ptr<ofxAssetManager> assets) {
			ArBoothGuiFactory::setup(config, assets);
			registerCreationFunction("positionSelectButton", [this](ofJson config, ofJson style) {return this->getPositionSelectButton(config, style); });
			registerCreationFunction("dashedCircle", [this](ofJson config, ofJson style) {return this->getDashedCircle(config, style); });
		}

		Node * CsarDfbGuiFactory::getPositionSelectButton(ofJson config, ofJson style)
		{
			PositionSelectButtonSettings s;
			readModalElementSettings(s, config, style);

			s.font = assets->getFonts();
			if (hasValue("font", config, style)) {
				s.style = assets->getFonts()->getStyle(getValue<string>("font", config, style));
			}

			if (hasValue("widthCircle", config, style)) {
				s.widthCircle = getValue<float>("widthCircle", config, style);
			}
			if (hasValue("colorActive2", config, style)) {
				s.colorActive2 = colors[getValue<string>("colorActive2", config, style)];
			}
			if (hasValue("colorActive3", config, style)) {
				s.colorActive3 = colors[getValue<string>("colorActive3", config, style)];
			}
			if (hasValue("tFadeIn", config, style)) {
				s.tFadeIn = getValue<float>("tFadeIn", config, style);
			}
			if (hasValue("tFadeOut", config, style)) {
				s.tFadeOut = getValue<float>("tFadeOut", config, style);
			}

			if (hasValue("text", config, style)) {
				s.text = getValue<string>("text", config, style);
			}
			if (hasValue("labelMargin", config, style)) {
				s.labelMargin = getValue<float>("labelMargin", config, style);
			}
			if (hasValue("labelHeight", config, style)) {
				s.labelHeight = getValue<float>("labelHeight", config, style);
			}

			if (hasValue("texture", config, style)) {
				s.texture = assets->getTexture(getValue<string>("texture", config, style));
			}

			PositionSelectButton* p = new PositionSelectButton();
			p->setup(s);
			return p;
		}
		Node * CsarDfbGuiFactory::getDashedCircle(ofJson config, ofJson style)
		{
			DashedCircleSettings s;
			readNodeSettings(s, config, style);

			if (hasValue("widthCircle", config, style)) {
				s.widthCircle = getValue<float>("widthCircle", config, style);
			}
			if (hasValue("color", config, style)) {
				s.color = colors[getValue<string>("color", config, style)];
			}

			if (hasValue("dashAngleStroke", config, style)) {
				s.dashAngleStroke = getValue<float>("dashAngleStroke", config, style);
			}
			if (hasValue("dashAngleEmpty", config, style)) {
				s.dashAngleEmpty = getValue<float>("dashAngleEmpty", config, style);
			}

			DashedCircle* p = new DashedCircle();
			p->setup(s);
			return p;
		}
	}
}
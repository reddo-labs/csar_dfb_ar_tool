#include "PositionSelectButton.h"

namespace ofxInterface {
	namespace arBoothGui {
		PositionSelectButton::PositionSelectButton()
		{
		}
		PositionSelectButton::PositionSelectButton(const PositionSelectButton & mom) : ModalElement(mom)
		{
			text = mom.text;

			ring = mom.ring;

			fadeIn = ofxAnimatableFloat(mom.fadeIn);
			fadeOut = ofxAnimatableFloat(mom.fadeOut);
			lastPressedPosition = mom.lastPressedPosition;

			
			colorActive2 = mom.colorActive2;
			colorActive3 = mom.colorActive3;

			bgPosition = mom.bgPosition;

			labelMargin = mom.labelMargin;
			labelHeight = mom.labelHeight;
			texture = mom.texture;

			font = mom.font;

			createBg();

			
		}
		//PersonSelectButton::PersonSelectButton(const PersonSelectButton & b)
		//{
		//	Node::Node(b);
		//	//*texInactive = *(b.texInactive);
		//}

		PositionSelectButton::~PositionSelectButton()
		{
		}

		Node * PositionSelectButton::clone()
		{
			return new  PositionSelectButton(*this);
		}

		void PositionSelectButton::setup(PositionSelectButtonSettings settings)
		{
			settings.type = CHECKER;
			ModalElement::setup(settings);
			colorActive2 = settings.colorActive2;
			colorActive3 = settings.colorActive3;

			text = settings.text;
			texture = settings.texture;
			style = settings.style;

			labelMargin = settings.labelMargin;
			labelHeight = settings.labelHeight;

			font = settings.font;
			
			createBg();

			// animation
			fadeIn.setup();
			fadeIn.setDuration(float(settings.tFadeIn)*0.001);

			fadeOut.setup();
			fadeOut.animateFromTo(0, 255);
			fadeOut.setDuration(float(settings.tFadeOut)*0.001);
			
			// label
			Label* label = new Label();
			LabelSettings ls;
			ls.alignment = OF_ALIGN_HORZ_CENTER;
			ls.font = settings.font;
			ls.name = "label";
			ls.style = style;
			ls.position = ofVec2f(-getWidth()*0.1, getHeight() - settings.labelHeight);
			ls.size = ofVec2f(getWidth()*1.2, settings.labelHeight);
			ls.text = text;
			label->setup(ls);

			//addChild(label);


			ofxInterface::Node* clickLayer = new ofxInterface::Node();
			clickLayer->setSize(size);
			clickLayer->setName("click");
			clickLayer->setPlane(100);

			addChild(clickLayer);

			
			

			/*setTouchDownFunction([this](ofxInterface::TouchEvent& t) {
				t.lastSeenAbove = this;
				ofNotifyEvent(eventClick, t);
			});*/
		}

		void PositionSelectButton::setLabel(string text)
		{
			removeChild(getChildWithName("label"));
			Label* label = new Label();
			LabelSettings ls;
			ls.alignment = OF_ALIGN_HORZ_CENTER;
			ls.font = font;
			ls.style = style;
			ls.name = "label";
			ls.position = ofVec2f(0, 0.5*(getHeight() - labelHeight));
			ls.size = ofVec2f(getWidth(), labelHeight);
			ls.text = text;
			label->setup(ls);

			//addChild(label);
		}

		void PositionSelectButton::draw()
		{
			fadeIn.update(ofGetLastFrameTime());
			fadeOut.update(ofGetLastFrameTime());

			// draw empty ring
			ofSetColor(colorInactive);
			ofDrawCircle(size*0.5, size.x*0.5);
			ofSetColor(255);

			// draw full ring
			drawBg();

			//texture.draw(0, 0, size.x, size.y);
		}

		void PositionSelectButton::onStateChanged(bool & isActive)
		{

			if (isActive) {
				fadeIn.animateFromTo(0, getChildWithName("background")->getWidth()*1.5);
				//lastPressedPosition = toLocal(ofVec2f(ofGetMouseX(), ofGetMouseY()));

			}
			else {
				fadeOut.animateFromTo(255, 0);
			}
		}

		void PositionSelectButton::drawBg()
		{
			PositionSelectButtonBackground* bg = static_cast<PositionSelectButtonBackground*>(getChildWithName("background"));
			if (isSelected) {
				bg->mouse = lastPressedPosition;
				bg->radius = fadeIn.val();
				bg->alpha = 255;
			}
			else {
				bg->alpha = 0;// fadeOut.val();
			}
		}

		void PositionSelectButton::createBg()
		{

			removeChild(getChildWithName("background"));
			PositionSelectButtonBackgroundSettings s;
			float diameter = min(getWidth(), getHeight());
			s.size = ofVec2f(diameter, diameter);
			s.position = bgPosition;
			s.color1 = colorActive;
			s.color2 = colorActive2;
			s.color3 = colorActive3;
			s.name = "background";

			PositionSelectButtonBackground* b = new PositionSelectButtonBackground();
			b->setup(s);
			addChild(b);


			removeChild(getChildWithName("overlay"));
			TextureNode* overlay = new TextureNode();
			TextureNodeSettings s2;
			s2.size = size;
			s2.texture = texture;
			s2.plane = 15;
			s2.name = "overlay";
			overlay->setup(s2);
			addChild(overlay);
		}

		void PositionSelectButton::reset()
		{
			isSelected.disableEvents();
			isSelected = false;
			isSelected.enableEvents();


			fadeIn.reset();
			fadeOut.reset();

			PositionSelectButtonBackground* bg = static_cast<PositionSelectButtonBackground*>(getChildWithName("background"));
			bg->alpha = 0;
			bg->radius = 0;

			setEnabled(true);
		}

		void PositionSelectButton::startBgAnimationFromCenter()
		{
			lastPressedPosition = size * 0.5;
		}

		void PositionSelectButton::createTouchListener()
		{

			getChildWithName("click")->setTouchDownFunction([this](ofxInterface::TouchEvent& t) {
				lastPressedPosition = toLocal(t.position);


				t.lastSeenAbove = this;
				onTouchDown(t);
				//ofNotifyEvent(eventClick, t);
			});
		}

		
		PositionSelectButtonBackground::PositionSelectButtonBackground()
		{
		}

		void PositionSelectButtonBackground::setup(PositionSelectButtonBackgroundSettings s)
		{
			Node::setup(s);
			color1 = s.color1;
			color2 = s.color2;
			color3 = s.color3;

			shader = getGradientCircularMaskShader();

			float diameter = min(getWidth(), getHeight());
			fbo.allocate(diameter, diameter);
			fbo.begin();
			ofClear(0, 0);
			fbo.end();
		}

		void PositionSelectButtonBackground::draw()
		{
			ofEnableBlendMode(OF_BLENDMODE_DISABLED);
			fbo.begin();

			ofClear(0, 0);
			shader.begin();

			shader.setUniform2f("begin", ofVec2f(0, 0));
			shader.setUniform2f("end", ofVec2f(1.0, 0.0));
			shader.setUniform1f("mid", 0.66f);
			shader.setUniform2f("size", getSize());
			shader.setUniform1f("radius", radius);
			shader.setUniform2f("mouse", mouse.get());

			shader.setUniform4f("color1", colorToVec4(color1));
			shader.setUniform4f("color2", colorToVec4(color2));
			shader.setUniform4f("color3", colorToVec4(color3));

			ofSetCircleResolution(200);
			ofDrawCircle(fbo.getWidth()*0.5, fbo.getHeight()*0.5, fbo.getWidth()*0.5);
			shader.end();

			fbo.end();
			ofEnableBlendMode(OF_BLENDMODE_ALPHA);

			ofSetColor(255, alpha);
			fbo.draw(0, 0);
		}


}
}

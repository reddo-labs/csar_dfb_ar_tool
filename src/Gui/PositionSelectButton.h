#pragma once

#include "ModalElement.h"
#include "Label.h"
#include "TextureNode.h"
#include "GradientShader.h"
#include "ofxAnimatableFloat.h"
#include "GPF.h"
#include "ofxFontStash2.h"



namespace ofxInterface {
	namespace arBoothGui {
		struct PositionSelectButtonBackgroundSettings : public NodeSettings {
			ofColor color1;
			ofColor color2;
			ofColor color3;
		};

		class PositionSelectButtonBackground : public Node
		{
		public:
			PositionSelectButtonBackground();
			void setup(PositionSelectButtonBackgroundSettings s);
			virtual void draw();

			ofParameter<ofVec2f> mouse;
			ofParameter<float> radius;
			ofParameter<int> alpha;

		private:
			ofColor color1;
			ofColor color2;
			ofColor color3;

			ofFbo fbo;
			ofShader shader;
		};

		struct PositionSelectButtonSettings : ModalElementSettings {
			// bg
			float widthCircle = 4;
			ofColor colorActive2 = ofColor(180);
			ofColor colorActive3 = ofColor(220);
			int tFadeIn = 500;
			int tFadeOut = 70;

			// label
			shared_ptr<ofxFontStash2::Fonts> font;
			ofTexture texture;
			ofxFontStash2::Style style;
			string text = "";
			
			float labelMargin;
			float labelHeight;
		};

		class PositionSelectButton :public ModalElement
		{
		public:
			PositionSelectButton();
			PositionSelectButton(const PositionSelectButton& mom);
			~PositionSelectButton();

			virtual Node* clone() override;

			void setup(PositionSelectButtonSettings settings);
			
			void setLabel(string text);
			
			virtual void draw();

			virtual void onStateChanged(bool& isActive) override;

			virtual void drawBg();

			void createBg();
			void reset();
			void startBgAnimationFromCenter();
			void createTouchListener();

			ofVec2f lastPressedPosition;

		private:
			ofPath ring;
			ofxAnimatableFloat fadeIn;
			ofxAnimatableFloat fadeOut;
			

			ofColor colorActive2;
			ofColor colorActive3;
			shared_ptr<ofxFontStash2::Fonts> font;


			ofTexture texture;
			ofxFontStash2::Style style;
			string text = "";
			
			ofVec2f bgPosition;
			
			float labelMargin;
			float labelHeight;
		};
	}
}

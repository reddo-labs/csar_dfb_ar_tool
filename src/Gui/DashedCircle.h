#pragma once

#include "ModalElement.h"
#include "TextField.h"
#include "TextureNode.h"
#include "HapPlayerNode.h"
#include "GradientShader.h"
#include "ofxAnimatableFloat.h"
#include "GPF.h"


namespace ofxInterface {
	namespace arBoothGui {
		struct DashedCircleSettings : NodeSettings {
			float widthCircle = 4;
			ofColor color = ofColor(180);
			float dashAngleStroke = 3.0;
			float dashAngleEmpty = 3.0;
		};

		class DashedCircle : public Node
		{
		public:
			DashedCircle() {};
			void setup(DashedCircleSettings s);
			virtual void draw();
			void onSizeChanged(Node& n);

		protected:
			void initRing();

		private:
			float widthCircle ;
			ofColor color;
			float dashAngleStroke;
			float dashAngleEmpty;

			ofPath ring;
		};

		

	
	}
}

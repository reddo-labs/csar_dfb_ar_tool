#pragma once
#include "ArBoothGuiFactory.h"
#include "PositionSelectButton.h"
#include "DashedCircle.h"

namespace ofxInterface {
	namespace arBoothGui {
		class CsarDfbGuiFactory : public ArBoothGuiFactory
		{
		public:
			virtual void setup(ofJson config, shared_ptr<ofxAssetManager> assets) override;
			Node* getPositionSelectButton(ofJson config, ofJson style);
			Node* getDashedCircle(ofJson config, ofJson style);
		};
	}
}



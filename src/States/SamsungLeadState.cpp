#include "SamsungLeadState.h"

//using namespace ofxInterface::reddoGui;
namespace reddo {
	namespace phototool {
		SamsungLeadState::SamsungLeadState() {
		}


		SamsungLeadState::~SamsungLeadState() {
		}

		void SamsungLeadState::activate() {
			PhotoToolState::activate();
			if (node->getChildWithName("dataProtection") != nullptr) {
				node->getChildWithName("dataProtection")->deactivate();
			}
			
			node->getChildWithName("checkerDaddy")->deactivate();

			static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerData"))->isSelected = false;
			static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerAbove16"))->isSelected = false;
			static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerUnder16"))->isSelected = false;
			static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerDaddy"))->isSelected = false;
			static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerFurtherUse"))->isSelected = false;

			static_cast<ofxInterface::reddoGui::Button*>(node->getChildWithName("next"))->setEnabled(false);

			static_cast<ofxInterface::ScrollableContainer*>(node->getChildWithName("scrollbar"))->getScrollableArea()->setPosition(0, 0);

			canvasControl->setPublicUse(false);
		}

		void SamsungLeadState::onStateEnd() {
			ofNotifyEvent(stateFinishedEvent, nextState);
		}

		void SamsungLeadState::onAbove16Clicked(ofxInterface::TouchEvent & t) {
			if (static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerAbove16"))->isSelected) {
				static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerUnder16"))->isSelected = false;
				node->getChildWithName("checkerDaddy")->deactivate();
			}
			checkReady();
		}

		void SamsungLeadState::onUnder16Clicked(ofxInterface::TouchEvent & t) {
			if (static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerUnder16"))->isSelected) {
				static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerAbove16"))->isSelected = false;
				node->getChildWithName("checkerDaddy")->activate();
			} else {
				node->getChildWithName("checkerDaddy")->deactivate();
				static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerDaddy"))->isSelected = false;
			}
			checkReady();
		}


		void SamsungLeadState::initState(PhotoToolStateConfig config) {
			// set up the callbacks

			node->getChildWithName("checkerData")->setClickFunction([this](ofxInterface::TouchEvent& t) {
				checkReady();
			});

			// activate data protection additional site if available
			if (node->getChildWithName("dataProtection") != nullptr) {
				node->getChildWithName("checkerData")->getChildWithName("labelData")->setTouchDownFunction([this](ofxInterface::TouchEvent& t) {
					node->getChildWithName("dataProtection")->activate();
				});
				node->getChildWithName("checkerData")->getChildWithName("hitboxData")->setTouchDownFunction([this](ofxInterface::TouchEvent& t) {
					node->getChildWithName("dataProtection")->activate();
				});
				node->getChildWithName("dataProtection")->getChildWithName("scrollableArea")->getChildWithName("exit")->setTouchDownFunction(
					[this](ofxInterface::TouchEvent& t) {
					node->getChildWithName("dataProtection")->deactivate();
				});
			}
			// else use label to activate / deactivate checker
			else {
				node->getChildWithName("checkerData")->getChildWithName("labelData")->setClickFunction([this](ofxInterface::TouchEvent& t) {
					auto ch = static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerData"));
					ch->isSelected = !ch->isSelected;
					checkReady();
				});
				node->getChildWithName("checkerData")->getChildWithName("hitboxData")->setClickFunction([this](ofxInterface::TouchEvent& t) {
					auto ch = static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerData"));
					ch->isSelected = !ch->isSelected;
					checkReady();
				});
			}
			
			ofAddListener(node->getChildWithName("checkerAbove16")->eventClick, this, &SamsungLeadState::onAbove16Clicked);
			node->getChildWithName("checkerAbove16")->getChildWithName("labelAbove16")->setClickFunction([this](ofxInterface::TouchEvent& t) {
				
				auto ch = static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerAbove16"));
				ch->isSelected = !ch->isSelected;
				onAbove16Clicked(t);
			});
			node->getChildWithName("checkerAbove16")->getChildWithName("hitboxAbove16")->setClickFunction([this](ofxInterface::TouchEvent& t) {

				auto ch = static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerAbove16"));
				ch->isSelected = !ch->isSelected;
				onAbove16Clicked(t);
			});

			ofAddListener(node->getChildWithName("checkerUnder16")->eventClick, this, &SamsungLeadState::onUnder16Clicked);
			ofAddListener(node->getChildWithName("checkerUnder16")->getChildWithName("labelUnder16")->eventClick, this, &SamsungLeadState::onUnder16Clicked);
			node->getChildWithName("checkerUnder16")->getChildWithName("labelUnder16")->setClickFunction([this](ofxInterface::TouchEvent& t) {
				auto ch = static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerUnder16"));
				ch->isSelected = !ch->isSelected;
				onUnder16Clicked(t);
			});
			node->getChildWithName("checkerUnder16")->getChildWithName("hitboxUnder16")->setClickFunction([this](ofxInterface::TouchEvent& t) {
				auto ch = static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerUnder16"));
				ch->isSelected = !ch->isSelected;
				onUnder16Clicked(t);
			});

			node->getChildWithName("checkerDaddy")->setClickFunction([this](ofxInterface::TouchEvent& t) {
				checkReady();
			});
			node->getChildWithName("checkerDaddy")->getChildWithName("labelDaddy")->setClickFunction([this](ofxInterface::TouchEvent& t) {
				auto ch = static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerDaddy"));
				ch->isSelected = !ch->isSelected;
				checkReady();
			});
			node->getChildWithName("checkerDaddy")->getChildWithName("hitboxDaddy")->setClickFunction([this](ofxInterface::TouchEvent& t) {
				auto ch = static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerDaddy"));
				ch->isSelected = !ch->isSelected;
				checkReady();
			});

			if (!toolSettings["config"]["lead"]["furtherUse"].get<bool>()) {
				node->getChildWithName("checkerFurtherUse")->deactivate();
			} else {
				node->getChildWithName("checkerFurtherUse")->setClickFunction([this](ofxInterface::TouchEvent& t) {
					canvasControl->setPublicUse(static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerFurtherUse"))->isSelected);
				});
				node->getChildWithName("checkerFurtherUse")->getChildWithName("labelFurtherUse")->setClickFunction([this](ofxInterface::TouchEvent& t) {
					auto ch = static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerFurtherUse"));
					ch->isSelected = !ch->isSelected;
					canvasControl->setPublicUse(static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerFurtherUse"))->isSelected);
				});
			}

			// callbacks next and back
			node->getChildWithName("next")->setClickFunction([this](ofxInterface::TouchEvent& t) {
				ofNotifyEvent(stateFinishedEvent, nextState);
			});

			node->getChildWithName("back")->setClickFunction([this](ofxInterface::TouchEvent& t) {
				string n = "idle";
				ofNotifyEvent(stateFinishedEvent, n);
			});
		}

		void SamsungLeadState::checkReady() {

				if (static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerAbove16"))->isSelected) {
					static_cast<ofxInterface::reddoGui::Button*>(node->getChildWithName("next"))->setEnabled(true);
				} else if (static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerUnder16"))->isSelected &&
					static_cast<ofxInterface::SimpleChecker*>(node->getChildWithName("checkerDaddy"))->isSelected) {
					static_cast<ofxInterface::reddoGui::Button*>(node->getChildWithName("next"))->setEnabled(true);
				} else {
					static_cast<ofxInterface::reddoGui::Button*>(node->getChildWithName("next"))->setEnabled(false);
				}

		}
	}
}
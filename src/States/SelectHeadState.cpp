#include "SelectHeadState.h"

namespace reddo {
	namespace phototool {

		SelectHeadState::SelectHeadState() {
		}


		SelectHeadState::~SelectHeadState() {
		}

		void SelectHeadState::activate() {
			PhotoToolState::activate();
			//node->getChildWithName("flash")->activate();

			cameraControl->stopStream();

			//animationPhoto.setInitialValue(node->getHeight());
			setInitialAnimationValues();
			animationFrame.startPlaying();
			animationCanvas.startPlaying();

			if (toolSettings["picture"]["mode"].get<string>() == "movie") {
				canvasControl->loadVideoInLayer("photo", "capture.mov");
				if (canvasControl->hasBackground()) {
					canvasControl->loadVideoInLayer("background", "capture1.mov");
					canvasControl->setBackgroundEnabled(true);
				}
				if (canvasControl->hasOverlay()) {
					canvasControl->loadVideoInLayer("overlay", "capture2.mov");
					canvasControl->setOverlayEnabled(true);
				}
			}
			else {
				canvasControl->stopPlayback();
			}

			canvasControl->resetPrints();

			interfaceHeadRevealed = false;
			isHeadModified = false;
			node->getChildWithName("interface_head")->deactivate();
			static_cast<ofxInterface::ModalElement*>(node->getChildWithName("interface_head")->getChildWithName("next"))->setEnabled(false);
			node->getChildWithName("circle")->deactivate();
			node->getChildWithName("interface_check")->deactivate();

			//unrotate
			glm::quat quat = glm::angleAxis(ofDegToRad(0), glm::vec3(0.0, 1.0, 0.0));
			auto wrapper = node->getChildWithName("rotateWrapper");
			wrapper->setGlobalOrientation(quat);
			//wrapper->setPosition(wrapper->getPosition().x, wrapper->getPosition().y, -100);

			// disable lighting
			notifyEvent("led_fadeOut");

			// enable players
			for (auto& p : canvasControl->getCanvas()->getChildWithName("overlay")->getChildWithName("player")->getChildren()) {
				p->activate();
			}
		}

		void SelectHeadState::update() {
			canvasControl->updateAnimation();
			rotateTimer.update();

			//animationFlash.update(ofGetLastFrameTime());
			//if (animationFlash.isPlaying()) {
			//	auto flash = static_cast<ofxInterface::ColorPanel*>(node->getChildWithName("flash"));
			//	ofColor c = flash->getColor();
			//	c.a = flashStartAlpha * animationFlash.getCurrentValue();
			//	flash->setColor(c);
			//}

			animationFrame.update(ofGetLastFrameTime());
			animationCanvas.update(ofGetLastFrameTime());
			if (animationFrame["x"].isPlaying()) {
				node->getChildWithName("frame")->setPosition(animationFrame["x"].getCurrentValue(), animationFrame["y"].getCurrentValue());
				node->getChildWithName("frame")->setSize(animationFrame["w"].getCurrentValue(), animationFrame["h"].getCurrentValue());
				node->getChildWithName("frame")->getChildWithName("canvas")->setPosition(animationCanvas["x"].getCurrentValue(), animationCanvas["y"].getCurrentValue());
				node->getChildWithName("frame")->getChildWithName("canvas")->setSize(animationCanvas["w"].getCurrentValue(), animationCanvas["h"].getCurrentValue());
			}
			else if(!interfaceHeadRevealed){
				
				node->getChildWithName("interface_head")->activate();
				interfaceHeadRevealed = true;
			}

			if (rotateTimer.isRunning())
			{
				float per = rotateTimer.getPercentage();
				float deg = 0;
				if (per < 0.5) {
					deg = ofxeasing::map_clamp(rotateTimer.getPercentage(), 0, 1, 0, 180, ofxeasing::sine::easeInOut);
				}
				else {
					if (!isHeadModified) {
						proceedImage();
						isHeadModified = true;
					}
					deg = ofxeasing::map_clamp(rotateTimer.getPercentage(), 0, 1, 180, 360, ofxeasing::sine::easeInOut);
					
				}
				glm::quat quat = glm::angleAxis(ofDegToRad(deg), glm::vec3(0.0, 1.0, 0.0));
				node->getChildWithName("rotateWrapper")->setGlobalOrientation(quat);
			}
		}

		void SelectHeadState::onNextState(ofxInterface::TouchEvent & ev) {
			canvasControl->stopPlayback();
			notifyEvent("currentImageId", { {"id",ofxModulePictureExporter::fileCodeToFilename("[yy]-[mm]-[dd]-[s]")} });
			string next = "icons";
			ofNotifyEvent(stateFinishedEvent, next);
		}

		void SelectHeadState::onPreviousState(ofxInterface::TouchEvent & ev) {
			canvasControl->stopPlayback();
			string back = "selection";
			ofNotifyEvent(stateFinishedEvent, back);
		}

		void SelectHeadState::createComposition(ofxInterface::TouchEvent & ev)
		{
			
		}

		void SelectHeadState::onFlashFinished(ofxAnimatableQueue::EventArg & ev) {
			node->getChildWithName("flash")->deactivate();
		}

		void SelectHeadState::onSecretButtonLeftClicked(ofxInterface::TouchEvent & t) {
			lastClickedLeftButton = ofGetElapsedTimeMillis();
		}

		void SelectHeadState::onSecretButtonRightClicked(ofxInterface::TouchEvent & t) {
			if (ofGetElapsedTimeMillis() - lastClickedLeftButton < 5000) {
				string nextState = "idle";
				ofNotifyEvent(stateFinishedEvent, nextState);
			}
		}

		void SelectHeadState::proceedImage()
		{
			auto camLayer = canvasControl->getLayer("photo")->getChildWithName("node_photo");
			camLayer->setPosition(headPos);
			camLayer->setSize(camLayer->getSize()*headScale);

			canvasControl->getCanvas()->getChildWithName("background")->getChildWithName("team_bg")->activate();
			canvasControl->getCanvas()->getChildWithName("overlay")->getChildWithName("player")->activate();

			string player = canvasControl->getMetadata()["name"].get<string>();
			canvasControl->getCanvas()->getChildWithName("overlay")->getChildWithName("player")->getChildWithName(player)->deactivate();
			canvasControl->getCanvas()->getChildWithName("overlay")->getChildWithName("team_overlay")->activate();
			canvasControl->getCanvas()->getChildWithName("overlay")->getChildWithName("node_logo_team")->activate();
		}

		void SelectHeadState::onRotateComplete()
		{
			
			node->getChildWithName("interface_check")->activate();
		}

		void SelectHeadState::initState(PhotoToolStateConfig config) {
			ofAddListener(node->getChildWithName("interface_head")->getChildWithName("next")->eventClick, this, &SelectHeadState::createComposition);
			ofAddListener(node->getChildWithName("interface_check")->getChildWithName("next")->eventClick, this, &SelectHeadState::onNextState);
			ofAddListener(node->getChildWithName("interface_check")->getChildWithName("back")->eventClick, this, &SelectHeadState::onPreviousState);
			ofAddListener(node->getChildWithName("secretButtonLeft")->eventTouchDown, this, &SelectHeadState::onSecretButtonLeftClicked);
			ofAddListener(node->getChildWithName("secretButtonRight")->eventTouchDown, this, &SelectHeadState::onSecretButtonRightClicked);

			ofxInterface::Node* frame = node->getChildWithName("frame");

			

			float tStart = 1.0;
			float tAnim = 0.5;
			AnimCurve curve = AnimCurve::QUARTIC_EASE_OUT;

			
			animationFrame["x"].addTransition(frame->getChildWithName("posFrame")->getPosition().x, tStart, AnimCurve::LINEAR);
			animationFrame["x"].addTransition(frame->getPosition().x, tAnim, curve);
			animationFrame["x"].addTransition(frame->getPosition().x, 0.5, AnimCurve::LINEAR);

			
			animationFrame["y"].addTransition(frame->getChildWithName("posFrame")->getPosition().y, tStart, AnimCurve::LINEAR);
			animationFrame["y"].addTransition(frame->getPosition().y, tAnim, curve);
			animationFrame["y"].addTransition(frame->getPosition().y, 0.5, AnimCurve::LINEAR);

			
			animationFrame["w"].addTransition(frame->getChildWithName("posFrame")->getWidth(), tStart, AnimCurve::LINEAR);
			animationFrame["w"].addTransition(frame->getWidth(), tAnim, curve);
			animationFrame["w"].addTransition(frame->getWidth(), 0.5, AnimCurve::LINEAR);

			
			animationFrame["h"].addTransition(frame->getChildWithName("posFrame")->getHeight(), tStart, AnimCurve::LINEAR);
			animationFrame["h"].addTransition(frame->getHeight(), tAnim, curve);
			animationFrame["h"].addTransition(frame->getHeight(), 0.5, AnimCurve::LINEAR);

			
			animationCanvas["x"].addTransition(frame->getChildWithName("posCanvas")->getPosition().x, tStart, AnimCurve::LINEAR);
			animationCanvas["x"].addTransition(frame->getChildWithName("canvas")->getPosition().x, tAnim, curve);
			animationCanvas["x"].addTransition(frame->getChildWithName("canvas")->getPosition().x, 0.5, AnimCurve::LINEAR);

			
			animationCanvas["y"].addTransition(frame->getChildWithName("posCanvas")->getPosition().y, tStart, AnimCurve::LINEAR);
			animationCanvas["y"].addTransition(frame->getChildWithName("canvas")->getPosition().y, tAnim, curve);
			animationCanvas["y"].addTransition(frame->getChildWithName("canvas")->getPosition().y, 0.5, AnimCurve::LINEAR);

			
			animationCanvas["w"].addTransition(frame->getChildWithName("posCanvas")->getWidth(), tStart, AnimCurve::LINEAR);
			animationCanvas["w"].addTransition(frame->getChildWithName("canvas")->getWidth(), tAnim, curve);
			animationCanvas["w"].addTransition(frame->getChildWithName("canvas")->getWidth(), 0.5, AnimCurve::LINEAR);

			
			animationCanvas["h"].addTransition(frame->getChildWithName("posCanvas")->getHeight(), tStart, AnimCurve::LINEAR);
			animationCanvas["h"].addTransition(frame->getChildWithName("canvas")->getHeight(), tAnim, curve);
			animationCanvas["h"].addTransition(frame->getChildWithName("canvas")->getHeight(), 0.5, AnimCurve::LINEAR);

			rotateTimer.setCountdown(toolSettings["scenes"]["team"]["rotationDuration"].get<int>());


			// add selection
			ofxInterface::Node* touchPanel = new ofxInterface::Node();
			touchPanel->setPosition(frame->getChildWithName("canvas")->getPosition());
			touchPanel->setSize(frame->getChildWithName("canvas")->getSize());
			touchPanel->setPlane(1000);
			touchPanel->setName("touch");
			node->addChild(touchPanel);

			auto circleScale = touchPanel->getSize() / canvasControl->getSize()/ toolSettings["scenes"]["team"]["scaleHead"].get<float>();
			circleScale.y = circleScale.x;
			auto newSizeCircle = ofVec2f(toolSettings["scenes"]["team"]["radiusCircle"].get<float>()) *circleScale;
			node->getChildWithName("circle")->setSize(newSizeCircle);

			touchPanel->setTouchDownFunction([this, touchPanel](ofxInterface::TouchEvent& t) {

				if (!isHeadModified) {
					auto circle = node->getChildWithName("circle");
					// set circle to border
					ofVec2f pMin = ofVec2f(touchPanel->getPosition()) + circle->getSize()*0.5;
					ofVec2f pMax = ofVec2f(touchPanel->getPosition()) + touchPanel->getSize() - circle->getSize()*0.5;
					t.position.x = ofClamp(t.position.x, pMin.x, pMax.x);
					t.position.y = ofClamp(t.position.y, pMin.y, pMax.y);

					// get position on canvas
					ofVec3f position = (t.position - touchPanel->getPosition() - circle->getSize()*0.5) / touchPanel->getSize();

					// convert 2:3 click to 9:16 click
					position.x *= canvasControl->getWidth();
					position.y = ofMap(position.y, 0, 1, 150, 1920, true);
					
					// scale camera layer
					headScale = toolSettings["scenes"]["team"]["scaleHead"].get<float>();
					position *= headScale;
					

					// get new position
					auto m = canvasControl->getMetadata()["position"];
					ofVec3f newPos = ofVec3f(m[0].get<float>(), m[1].get<float>());

					
					headPos = newPos - position;
					
					isHeadModified = false;

					static_cast<ofxInterface::ModalElement*>(node->getChildWithName("interface_head")->getChildWithName("next"))->setEnabled(true);


					circle->activate();
					circle->setPosition(t.position - circle->getSize()*0.5);
				}
			});


			


			node->getChildWithName("interface_head")->getChildWithName("next")->setTouchDownFunction([this](ofxInterface::TouchEvent& t) {
				
				rotateTimer.start();
				node->getChildWithName("circle")->deactivate();
				node->getChildWithName("interface_head")->deactivate();

				//auto wrapper = node->getChildWithName("rotateWrapper");
				//wrapper->setPosition(wrapper->getPosition().x, wrapper->getPosition().y, wrapper->getPosition().z-10);
				//cout << "pos " << wrapper->getPosition().z << endl;
			});

			ofAddListener(rotateTimer.timerFinishedEvent, this, &SelectHeadState::onRotateComplete);
			
		}

		void SelectHeadState::setInitialAnimationValues()
		{
			ofxInterface::Node* frame = node->getChildWithName("frame");
			animationFrame["x"].setInitialValue(frame->getChildWithName("posFrame")->getPosition().x);
			animationFrame["y"].setInitialValue(frame->getChildWithName("posFrame")->getPosition().y);
			animationFrame["w"].setInitialValue(frame->getChildWithName("posFrame")->getWidth());
			animationFrame["h"].setInitialValue(frame->getChildWithName("posFrame")->getHeight());
			animationCanvas["x"].setInitialValue(frame->getChildWithName("posCanvas")->getPosition().x);
			animationCanvas["y"].setInitialValue(frame->getChildWithName("posCanvas")->getPosition().y);
			animationCanvas["w"].setInitialValue(frame->getChildWithName("posCanvas")->getWidth());
			animationCanvas["h"].setInitialValue(frame->getChildWithName("posCanvas")->getHeight());
		}




		

}
}
#pragma once
#include "PhotoToolState.h"
#include "Ring.h"
#include "GPF.h"
#include "HapPlayerNode.h"
#include "SimpleTimer.h"

#include "ofxAnimatableFloat.h"

namespace reddo {
	namespace phototool {

		enum Playmode {
			IDLE,
			COMMERCIAL
		};

		class IdleState : public PhotoToolState
		{
		public:
			IdleState();
			~IdleState();

			void update() override;

			void activate() override;
			void onStateEnd(ofxInterface::TouchEvent & t);

			void onSecretButtonLeftClicked(ofxInterface::TouchEvent& t);
			void onSecretButtonRightClicked(ofxInterface::TouchEvent& t);
			void onVideoDelayFinished();

			void loadNewRingAnimation();
			void loadNewVideo();


		protected:
			virtual void initState(PhotoToolStateConfig config);
			void createRingQueue();
			void createVideoQueue();
			void createCommercialQueue();
			void setGuiElementsVisible(bool isActivated);

		private:
			long lastClickedLeftButton = 0;
			Playmode currentPlaymode = IDLE;

			// animated rings
			vector<string> ringList;
			vector<string> ringQueue;

			// videos
			vector<string> videoList;
			vector<string> videoQueue;

			vector<string> commercialList;
			vector<string> commercialQueue;

			ofxInterface::reddoGui::HapPlayerNode* video;
			reddo::SimpleTimer videoDelay;
		};

	}
}
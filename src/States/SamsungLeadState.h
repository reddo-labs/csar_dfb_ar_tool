#pragma once
#include "PhotoToolState.h"
#include "Button.h"
#include "SimpleChecker.h"

namespace reddo {
	namespace phototool {
		class SamsungLeadState : public PhotoToolState
		{
		public:
			SamsungLeadState();
			~SamsungLeadState();

			void activate() override;
			void onStateEnd();

			void onAbove16Clicked(ofxInterface::TouchEvent& t);
			void onUnder16Clicked(ofxInterface::TouchEvent& t);

		protected:
			virtual void initState(PhotoToolStateConfig config);
			void checkReady();

		private:
			long lastClickedLeftButton = 0;
		};

	}
}

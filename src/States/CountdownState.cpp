#include "CountdownState.h"

namespace reddo {
	namespace phototool {

		CountdownState::CountdownState() {
		}


		CountdownState::~CountdownState() {
		}

		void CountdownState::update() {
			currentStates.front().timer.update();
			activateImageTimer.update();

			float tAnimation = 250;

			// animation of text states
			if (ofIsStringInString(currentStates.front().id, "text")) {
				auto n = node->getChildWithName(currentStates.front().id);
				
				// use inverted styleset
				string text1 = "text";
				string text2 = "text2";

				if (canvasControl->getMetadata()["scene"] == "selfie") {
					n->getChildWithName("blockShadowi")->activate();
					n->getChildWithName("texti")->activate();
					n->getChildWithName("text2i")->activate();

					n->getChildWithName("blockShadow")->deactivate();
					n->getChildWithName("text")->deactivate();
					n->getChildWithName("text2")->deactivate();

					text1 = "texti";
					text2 = "text2i";
				} else {
					n->getChildWithName("blockShadowi")->deactivate();
					n->getChildWithName("texti")->deactivate();
					n->getChildWithName("text2i")->deactivate();

					n->getChildWithName("blockShadow")->activate();
					n->getChildWithName("text")->activate();
					n->getChildWithName("text2")->activate();
				}

				float fontSize = ofxeasing::map_clamp(currentStates.front().timer.getRunTime(), 0.0, tAnimation, numberFontSize*1.5, textFontSize[currentStates.front().id], ofxeasing::cubic::easeOut);
				ofxInterface::Label* l1 = static_cast<ofxInterface::Label*>(n->getChildWithName(text1));
				l1->setFontSize(fontSize);
				ofColor c1 = l1->getColor();
				c1.a = ofxeasing::map_clamp(currentStates.front().timer.getRunTime(), 0.1, tAnimation, 0,255, ofxeasing::cubic::easeOut);
				l1->setColor(c1);
				
				int fontSize2 = ofxeasing::map_clamp(currentStates.front().timer.getRunTime(), 0.0, tAnimation, numberFontSize*2.5, textFontSize[currentStates.front().id + "1"], ofxeasing::cubic::easeOut);
				static_cast<ofxInterface::Label*>(n->getChildWithName(text2))->setFontSize(fontSize2);
				static_cast<ofxInterface::Label*>(n->getChildWithName(text2))->setColor(c1);

				n->setPosition(n->getPosition().x, ofxeasing::map_clamp(currentStates.front().timer.getRunTime(), 0.0, tAnimation, -n->getHeight(), textFontPos[currentStates.front().id], ofxeasing::cubic::easeOut));

			}
			// animation of video state
			else if (currentStates.front().id == "videoPlay") {
				if (currentStates.front().timer.getRunTime() > 2500) {
					node->getChildWithName("videoPlay")->deactivate();
				}
			}
			// animation of ring states
			else if (ofIsStringInString(currentStates.front().id, "ring")) {
				for (auto& ani : aniRings) {
					ani.update(ofGetLastFrameTime());
				}
				auto s = node->getChildWithName(currentStates.front().id);

				for (size_t i = 0; i < 4; i++)
				{
					s->getChildWithName("r" + ofToString(i))->setPosition(aniRings[i]["x"].getCurrentValue(), aniRings[i]["y"].getCurrentValue());
					s->getChildWithName("r" + ofToString(i))->setSize(aniRings[i]["w"].getCurrentValue(), aniRings[i]["h"].getCurrentValue());
				}

				

				if (currentStates.front().id == "ring0") {
					auto n = node->getChildWithName("ring0")->getChildWithName("textPanel");
					n->setPosition(ofxeasing::map_clamp(currentStates.front().timer.getPercentage(), 0.0, 0.15,-node->getWidth(), 0, ofxeasing::cubic::easeOut), 
							n->getPosition().y);
				} else {
					float fontSize = 0;
					if (currentStates.front().timer.getPercentage() > 0.35) {
						fontSize = ofxeasing::map_clamp(currentStates.front().timer.getPercentage(), 0.5, 0.65, numberFontSize*1.15, numberFontSize, ofxeasing::cubic::easeOut);
					}
					static_cast<ofxInterface::Label*>(node->getChildWithName(currentStates.front().id)->getChildWithName("number"))->setFontSize(fontSize);
				}
				

			}
		}

		void CountdownState::activate() {
			PhotoToolState::activate();

			canvasControl->stopOverlayVideo("node_overlay");
			canvasControl->stopBackgroundVideo("node_background");

			canvasControl->setImageEnabled(false);
			canvasControl->stopPlayback();
			cameraControl->startStream();

			// manage positions
			auto dCountdown = toolSettings["scenes"]["dImageCountdown"];

			string sceneId = canvasControl->getMetadata()["scene"].get<string>();
			auto camLayer = canvasControl->getCanvas()->getLayer("photo")->getChildWithName("node_photo");
			auto pos = toolSettings["scenes"][sceneId]["posCam"];
			float scale = toolSettings["scenes"][sceneId]["scaleCam"].get<float>();
			camLayer->setPosition(pos[0].get<float>() + dCountdown[0].get<float>(), pos[1].get<float>() + dCountdown[1].get<float>());
			camLayer->setSize(scale * layerSizes["photo"]);


			auto vidLayer = canvasControl->getCanvas()->getLayer("overlay")->getChildWithName("node_overlay");
			auto posVid = toolSettings["scenes"][sceneId]["posVideo"];
			float scaleVid = toolSettings["scenes"][sceneId]["scaleVideo"].get<float>();
			vidLayer->setPosition(posVid[0].get<float>() + dCountdown[0].get<float>(), posVid[1].get<float>() + dCountdown[1].get<float>());
			vidLayer->setSize(scaleVid* layerSizes["node_overlay"]);

			auto bgLayer = canvasControl->getCanvas()->getLayer("background")->getChildWithName("node_background");
			bgLayer->setPosition(posVid[0].get<float>() + dCountdown[0].get<float>(), posVid[1].get<float>() + dCountdown[1].get<float>());
			bgLayer->setSize(scaleVid* layerSizes["node_overlay"]);


			activateImageTimer.start();

			// set scene timers
			currentStates = countdownStates;
			string sceneName = canvasControl->getMetadata()["scene"].get<string>();
			for (auto& state : currentStates) {
				auto cValue = toolSettings["scenes"][sceneName]["timing"][state.id];
				if (cValue != nullptr) {
					state.timer.setCountdown(cValue.get<int>());
				}
			}

			for (auto& state : currentStates) {
				auto n = node->getChildWithName(state.id);
				if (n != nullptr) {
					n->deactivate();
				}
				ofAddListener(state.timer.timerFinishedEvent, this, &CountdownState::onCountdownOver);
			}

			for (auto& ani: aniRings){
				ani.startPlaying();
			}

			currentStates.front().timer.start();
			node->getChildWithName(currentStates.front().id)->activate();
			
			notifyEvent("led_ring_on");
			notifyEvent("led_fadeIn");
		}

		void CountdownState::onCountdownOver() {
			// switch nodes
			auto n = node->getChildWithName(currentStates.front().id);
			if (n != nullptr) {
				n->deactivate();
			}
			
			if (currentStates.front().id == "videoPlay") {
				canvasControl->stopOverlayVideo("node_overlay");
				canvasControl->stopBackgroundVideo("node_background");
			}

			currentStates.pop_front();

			if (currentStates.size() > 0) {
				if (currentStates.size() > 1) {
					// enable lighting animation
					if (currentStates.front().id == "ring2") {
						notifyEvent("on");
					}

					// restart ring animation
					if (ofIsStringInString(currentStates.front().id, "ring")) {
						for (size_t i = 0; i < 4; i++) {
							static_cast<ofxInterface::arBoothGui::Ring*>(node->getChildWithName(currentStates.front().id)->getChildWithName("r" + ofToString(i)))->setStartTime(ofGetElapsedTimeMillis());
						}
					}
					else if (currentStates.front().id == "videoPlay") {
						if (canvasControl->getMetadata()["scene"] == "selfie") {
							canvasControl->getCanvas()->getChildWithName("background")->getChildWithName("node_background")->activate();
							canvasControl->getCanvas()->getChildWithName("overlay")->getChildWithName("node_overlay")->activate();
						}else if (canvasControl->getMetadata()["scene"] == "team") {

						}else if (canvasControl->getMetadata()["scene"] == "yogi") {
							canvasControl->getCanvas()->getChildWithName("overlay")->getChildWithName("node_overlay")->activate();
						}
						

						canvasControl->playOverlayVideo("node_overlay");
						canvasControl->playBackgroundVideo("node_background");
					}
				}
				// start flash
				else{
					// enable lighting
					

					if (toolSettings["picture"]["mode"].get<string>() == "movie") {
						ofNotifyEvent(stateFinishedEvent, nextState);
					}
					else {
						canvasControl->stopBackgroundVideo ("node_background");
						canvasControl->stopOverlayVideo("node_overlay");
						cameraControl->startCapture();
					}
				}

				currentStates.front().timer.start();
				auto n = node->getChildWithName(currentStates.front().id);
				if (n != nullptr) {
					n->activate();
				}
			}
			
		}

		void CountdownState::onEnableImage()
		{
			canvasControl->setImageEnabled(true);
		}

		void CountdownState::initState(PhotoToolStateConfig config) {
			countdownStates.push_back(CountdownPhase("text0", 4000)); // scene dependend
			countdownStates.push_back(CountdownPhase("text1", 4000));
			countdownStates.push_back(CountdownPhase("buffer", 3000));
			countdownStates.push_back(CountdownPhase("videoPlay", 0));// scene dependend
			countdownStates.push_back(CountdownPhase("buffer2", 1000));
			countdownStates.push_back(CountdownPhase("ring0", 2000));
			countdownStates.push_back(CountdownPhase("ring1", 1200));
			countdownStates.push_back(CountdownPhase("ring2", 1200));
			countdownStates.push_back(CountdownPhase("ring3", 1200));
			countdownStates.push_back(CountdownPhase("flash", 50));

			for (auto& c:countdownStates){
				countdownTimes.insert(make_pair(c.id, c.timer.getCountdown()));
			}

			// ring animation
			for (int i = 0; i < 4; ++i) {
				aniRings.push_back(PosSizeAnimationQueue());
				auto nMain = node->getChildWithName("ring0");
				aniRings.back().addNodeValues(nMain->getChildWithName("r" + ofToString(i)), 0, AnimCurve::LINEAR);
				aniRings.back().addNodeValues(nMain->getChildWithName("r" + ofToString(i)), float(countdownTimes["ring0"])*0.001, AnimCurve::LINEAR);
				for (int r = 1; r < 4; ++r) {
					auto lastNode = node->getChildWithName("ring" + ofToString(r-1))->getChildWithName("r" + ofToString(i));
					auto cNode = node->getChildWithName("ring" + ofToString(r))->getChildWithName("r" + ofToString(i));
					aniRings.back().addNodeValues(lastNode, 0, AnimCurve::LINEAR);
					aniRings.back().addNodeValues(cNode, float(countdownTimes["ring" + ofToString(r)]-700)*0.001, AnimCurve::QUADRATIC_EASE_OUT);
					aniRings.back().addNodeValues(cNode, float(700)*0.001, AnimCurve::LINEAR);
				}
			}

			// number animation 
			numberFontSize = static_cast<ofxInterface::Label*>(node->getChildWithName("ring1")->getChildWithName("number"))->getFontSize();
			textFontSize.insert(make_pair("text0", static_cast<ofxInterface::Label*>(node->getChildWithName("text0")->getChildWithName("text"))->getFontSize()));
			textFontSize.insert(make_pair("text01", static_cast<ofxInterface::Label*>(node->getChildWithName("text0")->getChildWithName("text2"))->getFontSize()));
			textFontSize.insert(make_pair("text1", static_cast<ofxInterface::Label*>(node->getChildWithName("text1")->getChildWithName("text"))->getFontSize()));
			textFontSize.insert(make_pair("text11", static_cast<ofxInterface::Label*>(node->getChildWithName("text1")->getChildWithName("text2"))->getFontSize()));

			textFontPos.insert(make_pair("text0", node->getChildWithName("text0")->getPosition().y));
			textFontPos.insert(make_pair("text1",node->getChildWithName("text1")->getPosition().y));


			// layer sizes
			layerSizes.insert(make_pair("node_overlay", canvasControl->getCanvas()->getLayer("overlay")->getChildWithName("node_overlay")->getSize()));
			layerSizes.insert(make_pair("photo", canvasControl->getCanvas()->getLayer("photo")->getSize()));


			activateImageTimer.setCountdown(500);
			ofAddListener(activateImageTimer.timerFinishedEvent, this, &CountdownState::onEnableImage);
		}

		void CountdownState::proceedModuleEvent(ModuleEvent & e)
		{
			if (e.address == "captured") {
				string nextState = "checkPhoto";
				if (canvasControl->getMetadata()["scene"] == "team") {
					 nextState = "selectHead";
				}
				else {
					canvasControl->getCanvas()->getChildWithName("overlay")->getChildWithName("node_logo_" + canvasControl->getMetadata()["scene"].get<string>())->activate();
				}

				//reset Position
				auto dCountdown = toolSettings["scenes"]["dImageCountdown"];

				string sceneId = canvasControl->getMetadata()["scene"].get<string>();
				auto camLayer = canvasControl->getCanvas()->getLayer("photo")->getChildWithName("node_photo");
				camLayer->setPosition(camLayer->getPosition() - ofVec3f( dCountdown[0].get<float>(), dCountdown[1].get<float>()));
				auto vidLayer = canvasControl->getCanvas()->getLayer("overlay")->getChildWithName("node_overlay");
				vidLayer->setPosition(vidLayer->getPosition() - ofVec3f(dCountdown[0].get<float>(), dCountdown[1].get<float>()));
				auto bgLayer = canvasControl->getCanvas()->getLayer("background")->getChildWithName("node_background");
				bgLayer->setPosition(bgLayer->getPosition() - ofVec3f(dCountdown[0].get<float>(), dCountdown[1].get<float>()));

				ofNotifyEvent(stateFinishedEvent, nextState);
			}
		}
	}
}
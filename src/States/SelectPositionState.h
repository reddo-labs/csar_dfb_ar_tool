#pragma once
#include "PhotoToolState.h"
#include "Coverflow.h"
#include "PositionSelectButton.h"
#include "SimpleTimer.h"
#include "ofxEasing.h"

namespace reddo {
	namespace phototool {
		class SelectPositionState : public PhotoToolState
		{
		public:
			SelectPositionState();
			~SelectPositionState();

			void update() override;

			void onStateEnd(ofxInterface::TouchEvent& ev);
			void activate() override;
			void onTimerEnd();

		protected:
			virtual void initState(PhotoToolStateConfig config);

		private:
			vector<ofxInterface::arBoothGui::PositionSelectButton*> buttons;
			SimpleTimer btnClickedTimer;
			SimpleTimer startAnimationTimer;
			
			vector<ofVec2f> buttonPositions;

		};
	}
}
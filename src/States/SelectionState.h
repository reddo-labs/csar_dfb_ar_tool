#pragma once
#include "PhotoToolState.h"
#include "Coverflow.h"
namespace reddo {
	namespace phototool {
		class SelectionState : public PhotoToolState
		{
		public:
			SelectionState();
			~SelectionState();

			void update() override;

			void onStateEnd(ofxInterface::TouchEvent& ev);
			void activate() override;

		protected:
			virtual void initState(PhotoToolStateConfig config);
			void loadVideos();

		private:
			vector<ofTexture> previews;
			vector<ofFbo> previewFbos;
			vector<ofTexture> texturesPointer;
		};
	}
}
#pragma once
#include "PhotoToolState.h"
#include "Label.h"
#include "ofxAnimatableQueue.h"
#include "ofxAnimatableFloat.h"
#include "PosAnimationQueue.h"

#include "SimpleTimer.h"
#include "ofxEasing.h"
#include "TextureNode.h"
#include "Ring.h"	

namespace reddo {
	class CountdownPhase {
	public:
		CountdownPhase(string id_, int runtime) {
			id = id_;
			timer.setCountdown(runtime);
		};
		reddo::SimpleTimer timer;
		string id;
	};

	namespace phototool {
		class CountdownState : public PhotoToolState
		{
		public:
			CountdownState();
			~CountdownState();

			void update() override;
			void activate() override;

			void onCountdownOver();
			void onEnableImage();


		protected:
			virtual void initState(PhotoToolStateConfig config);
			void proceedModuleEvent(ModuleEvent& e);

		private:
			// animation
			deque<CountdownPhase> countdownStates;
			deque<CountdownPhase> currentStates;
			map<string, int> countdownTimes;
			vector<PosSizeAnimationQueue> aniRings;

			float numberFontSize;
			map<string, float> textFontSize;
			map<string, float> textFontPos;
			map<string, ofVec2f> layerSizes;

			reddo::SimpleTimer activateImageTimer;

		};

	}
}
#include "SelectPositionState.h"
namespace reddo {
	namespace phototool {
		SelectPositionState::SelectPositionState() {
		}

		SelectPositionState::~SelectPositionState() {
		}

		void SelectPositionState::update() {
			btnClickedTimer.update();
			startAnimationTimer.update();

			if (startAnimationTimer.isRunning()) {
				float dy = node->getHeight()*0.05;
				for (size_t i = 0; i < buttons.size(); i++)
				{
					buttons[i]->setPosition(buttons[i]->getPosition().x, 
ofxeasing::map_clamp(startAnimationTimer.getPercentage(), 0, 1, buttonPositions[i].y - dy, buttonPositions[i].y,ofxeasing::cubic::easeIn));
				}
			}
		}

		void SelectPositionState::onStateEnd(ofxInterface::TouchEvent& ev) {
			ofNotifyEvent(stateFinishedEvent, nextState);
			
		}

		void SelectPositionState::activate() {
			ToolState::activate();
			
			for (auto& b : buttons)
			{
				b->isSelected = false;
			}

			startAnimationTimer.start();
		}

		void SelectPositionState::onTimerEnd()
		{
			ofNotifyEvent(stateFinishedEvent, nextState);
		}

		void SelectPositionState::initState(PhotoToolStateConfig config) {
			btnClickedTimer.setCountdown(1000);
			startAnimationTimer.setCountdown(300);
			ofAddListener(btnClickedTimer.timerFinishedEvent, this, &SelectPositionState::onTimerEnd);

			vector<string> files;
			files.push_back(toolSettings["scenes"]["yogi"]["sample"].get<string>());
			files.push_back(toolSettings["scenes"]["selfie"]["sample"].get<string>());
			files.push_back(toolSettings["scenes"]["team"]["sample"].get<string>());


			// get item templates
			ofxInterface::arBoothGui::PositionSelectButton* buttonTemplate = static_cast<ofxInterface::arBoothGui::PositionSelectButton*>(node->getChildWithName("itemTemplate"));
			ofVec2f scale = node->getSize() / canvasControl->getSize();
			ofVec2f dButton = buttonTemplate->getSize()*ofVec2f(15. / 180., 40. / 180.);

			// create buttons
			for (auto& buttonDesc: toolSettings["scenes"]["team"]["positions"])
			{
				ofxInterface::arBoothGui::PositionSelectButton* button = static_cast<ofxInterface::arBoothGui::PositionSelectButton*>(buttonTemplate->clone());
				button->activate();

				
				ofVec2f pos = ofVec2f(buttonDesc["position"][0].get<float>(), buttonDesc["position"][1].get<float>())*scale + dButton;

				button->setPosition(pos);
				button->setLabel(buttonDesc["description"].get<string>());
				button->setName(buttonDesc["description"].get<string>());
				//button->createTouchListener();
				buttons.push_back(button);
				node->addChild(buttons.back());
				

				

				button->getChildWithName("click")->setTouchDownFunction([this,button](ofxInterface::TouchEvent& t) {
					auto m = canvasControl->getMetadata();

					for (auto& buttonDesc : toolSettings["scenes"]["team"]["positions"])
					{
						if (buttonDesc["description"] == button->getName()) {
							m["position"] = buttonDesc["position"];
							m["name"] = buttonDesc["description"];
							canvasControl->setMetadata(m);
						}
					}

					button->lastPressedPosition = button->toLocal(t.position);

					for (auto& b:buttons)
					{
						if (b->isSelected) {
							b->isSelected = false;
						}
						
					}
					button->isSelected = true;
					btnClickedTimer.start();
					//node->getChildWithName("next")->activate();

					//notifyEvent(stateFinishedEvent, nextState);
				});

				buttonPositions.push_back(button->getPosition());
			}

			//auto btnNext = static_cast<ofxInterface::ModalElement*>(node->getChildWithName("next"));
			//btnNext->setEnabled(false);
			//ofAddListener(btnNext->eventTouchDown, this, &SelectPositionState::onStateEnd);
			/*btnNext->setTouchDownFunction([this](ofxInterface::TouchEvent& t) {
				onStateEnd(t);
				//ofNotifyEvent(stateFinishedEvent, nextState);
			});*/

			
		}

	}
}
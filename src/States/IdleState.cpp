#include "IdleState.h"

using namespace ofxInterface::reddoGui;

namespace reddo {
	namespace phototool {
		IdleState::IdleState() {
		}


		IdleState::~IdleState() {
		}

		void IdleState::update()
		{
			videoDelay.update();
			if (video->isLoaded() && video->getIsMovieDone() && !videoDelay.isRunning()) {
				if (currentPlaymode == COMMERCIAL) {
					setGuiElementsVisible(true);
				}
				
				video->setTexture(ofTexture());
				videoDelay.start();
			}
		}

		void IdleState::activate() {
			PhotoToolState::activate();
			cameraControl->stopStream();
			canvasControl->disableAllElements();
			canvasControl->getLayer("photo")->setPosition(ofVec2f(0));


			canvasControl->setBackgroundEnabled(false);
			canvasControl->setOverlayEnabled(false);
			canvasControl->getCanvas()->getChildWithName("overlay")->deactivate();

			// disable lighting
			notifyEvent("led_off");
			

			//loadNewRingAnimation();
			loadNewVideo();

		}

		void IdleState::onStateEnd(ofxInterface::TouchEvent & t) {
			video->close();
			ofNotifyEvent(stateFinishedEvent, nextState);
		}


		void IdleState::onSecretButtonLeftClicked(ofxInterface::TouchEvent & t) {
			lastClickedLeftButton = ofGetElapsedTimeMillis();
		}

		void IdleState::onSecretButtonRightClicked(ofxInterface::TouchEvent & t) {
			if (ofGetElapsedTimeMillis() - lastClickedLeftButton < 5000) {
				string nextState = "setup";
				ofNotifyEvent(stateFinishedEvent, nextState);
			}
		}

		void IdleState::onVideoDelayFinished()
		{
			loadNewVideo();
		}

		void IdleState::loadNewRingAnimation()
		{
			if (ringQueue.size() == 0) {
				createRingQueue();
			}
			for (auto& elem:ringList){
				node->getChildWithName(elem + "0")->deactivate();
				node->getChildWithName(elem + "1")->deactivate();
			}
			node->getChildWithName(ringQueue.back() + "0")->activate();
			node->getChildWithName(ringQueue.back() + "1")->activate();
			ringQueue.pop_back();
		}

		void IdleState::loadNewVideo()
		{
			if (currentPlaymode == IDLE && commercialList.size()>0) {
				currentPlaymode = COMMERCIAL;
				setGuiElementsVisible(false);
			}
			else {
				setGuiElementsVisible(true);
				currentPlaymode = IDLE;
			}

			videoDelay.stop();

			if (currentPlaymode == IDLE) {
				if (videoQueue.size() == 0) {
					createVideoQueue();
				}
				video->load(videoQueue.back());
			}
			else {
				if (commercialQueue.size() == 0) {
					createCommercialQueue();
				}
				video->load(commercialQueue.back());
			}

			video->setLoopState(ofLoopType::OF_LOOP_NONE);
			video->play();

			if (currentPlaymode == IDLE) {
				videoQueue.pop_back();
				
			}
			else {
				commercialQueue.pop_back();
			}
		}

		void IdleState::initState(PhotoToolStateConfig config) {
			ofAddListener(static_cast<ofxInterface::Node*>(node->getChildWithName("nextFull"))->eventClick, this, &IdleState::onStateEnd);
			ofAddListener(node->getChildWithName("secretButtonLeft")->eventTouchDown, this, &IdleState::onSecretButtonLeftClicked);
			ofAddListener(node->getChildWithName("secretButtonRight")->eventTouchDown, this, &IdleState::onSecretButtonRightClicked);

			// list ring nodes for animation
			int i = 0;
			while (node->getChildWithName("ring" + ofToString(i) + "0") != nullptr){
				ringList.push_back("ring" + ofToString(i));  
				ofAddListener(static_cast<ofxInterface::arBoothGui::Ring*>(node->getChildWithName("ring" + ofToString(i) + "0"))->animationCompleteEvent, this, &IdleState::loadNewRingAnimation);
				++i;
			}
			createRingQueue();

			// list videos for animation
			videoList = GPF::listFiles(config.toolSettings["animatedOverlay"]["videoStartScreen"]);
			commercialList = GPF::listFiles(config.toolSettings["animatedOverlay"]["videoCommercial"]);
			createVideoQueue();
			createCommercialQueue();

			video = static_cast<ofxInterface::reddoGui::HapPlayerNode*>(node->getChildWithName("video"));
			videoDelay.setCountdown(config.toolSettings["animatedOverlay"]["videoStartScreenDelay"].get<int>());
			ofAddListener(videoDelay.timerFinishedEvent, this, &IdleState::onVideoDelayFinished);
		}

		void IdleState::createRingQueue()
		{
			ringQueue = ringList;
			std::random_shuffle(ringQueue.begin(), ringQueue.end());
		}

		void IdleState::createVideoQueue()
		{
			videoQueue = videoList;
			std::random_shuffle(videoQueue.begin(), videoQueue.end());
		}
		void IdleState::createCommercialQueue()
		{
			commercialQueue = commercialList;
			std::random_shuffle(commercialQueue.begin(), commercialQueue.end());
		}
		void IdleState::setGuiElementsVisible(bool isActivated)
		{
			if (!isActivated) {
				//node->getChildWithName("logo")->deactivate();
				node->getChildWithName("headBlock")->deactivate();
				node->getChildWithName("head")->deactivate();
				node->getChildWithName("bgButtonIdle")->deactivate();
				node->getChildWithName("next")->deactivate();
				//node->getChildWithName("mask")->deactivate();
			}else {
				//node->getChildWithName("logo")->activate();
				node->getChildWithName("headBlock")->activate();
				node->getChildWithName("head")->activate();
				node->getChildWithName("bgButtonIdle")->activate();
				node->getChildWithName("next")->activate();
				//node->getChildWithName("mask")->activate();
			}
		}
	}
}
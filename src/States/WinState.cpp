#include "WinState.h"

using namespace ofxInterface::reddoGui;
namespace reddo {
	namespace phototool {
		WinState::WinState() {
		}


		WinState::~WinState() {
		}

		void WinState::activate() {
			PhotoToolState::activate();

			node->getChildWithName("overlay")->deactivate();
				



			node->getChildWithName("restart")->deactivate();
			node->getChildWithName("link")->deactivate();

			node->getChildWithName("wrapper")->setPosition(pWrapperStart);
			wrapperAni.animateFromTo(pWrapperStart.y, pWrapperEnd.y);

			logImage();

		}

		void WinState::onStateEnd() {
			ofNotifyEvent(stateFinishedEvent, nextState);
		}

		void WinState::update() {
			wrapperAni.update(ofGetLastFrameTime());
			node->getChildWithName("wrapper")->setPosition(pWrapperEnd.x, wrapperAni.val());
			if (!wrapperAni.isAnimating()) {
				node->getChildWithName("restart")->activate();
				node->getChildWithName("link")->activate();
			}
		}

		void WinState::logImage()
		{
			imageLog.open("imageLog.csv", ofFile::Append, false);
			imageLog << canvasControl->getMetadata()["scene"].get<string>() << "," << ofGetTimestampString() << "\n";
			imageLog.close();
		}

		void WinState::initState(PhotoToolStateConfig config) {
			node->getChildWithName("restart")->setClickFunction([this](ofxInterface::TouchEvent& t) {
				onStateEnd();
			});
			node->getChildWithName("link")->setClickFunction([this](ofxInterface::TouchEvent& t) {
				node->getChildWithName("overlay")->activate();
			});
			node->getChildWithName("overlay")->getChildWithName("close")->setClickFunction([this](ofxInterface::TouchEvent& t) {
				node->getChildWithName("overlay")->deactivate();
			});

			pWrapperStart = node->getChildWithName("wrapper")->getPosition();

			wrapperAni.setup();
			wrapperAni.setDuration(0.3);
			wrapperAni.setCurve(AnimCurve::CUBIC_EASE_IN);


			// init logFile
			imageLog.open("imageLog.csv", ofFile::Append, false);
			if (imageLog.getSize() == 0) {
				imageLog << "scene,timestamp\n";
			}
			imageLog.close();
		}

		void WinState::proceedModuleEvent(ModuleEvent & e)
		{
			
		}
		
	}
}
#pragma once
#include "PhotoToolState.h"
#include "Label.h"
#include "ofxAnimatableFloat.h"
#include "ofxAnimatableQueue.h"
#include "ColorPanel.h"
#include "HapPlayerNode.h"
#include "ofxModulePictureExporter.h"
#include "PosAnimationQueue.h"
#include "SimpleTimer.h"
#include "ofxEasing.h"

namespace reddo {
	namespace phototool {

		class SelectHeadState : public PhotoToolState
		{
		public:
			SelectHeadState();
			~SelectHeadState();

			void activate() override;
			void update() override;

			void onNextState(ofxInterface::TouchEvent& ev);
			void onPreviousState(ofxInterface::TouchEvent& ev);
			void createComposition(ofxInterface::TouchEvent& ev);

			void onFlashFinished(ofxAnimatableQueue::EventArg& ev);

			void onSecretButtonLeftClicked(ofxInterface::TouchEvent& t);
			void onSecretButtonRightClicked(ofxInterface::TouchEvent& t);

			void proceedImage();
			void onRotateComplete();

		protected:
			virtual void initState(PhotoToolStateConfig config);
			void setInitialAnimationValues();


		private:
			ofxAnimatableQueue animationFlash;
			int flashStartAlpha;

			PosSizeAnimationQueue animationFrame;
			PosSizeAnimationQueue animationCanvas;
			bool interfaceHeadRevealed = false;


			SimpleTimer rotateTimer;

			long lastClickedLeftButton = 0;



			bool isHeadModified = false;
			ofVec2f headPos;
			float headScale;

		};

	}
}
#include "SelectionState.h"
namespace reddo {
	namespace phototool {
		SelectionState::SelectionState() {
		}

		SelectionState::~SelectionState() {
		}

		void SelectionState::update() {
		}

		void SelectionState::onStateEnd(ofxInterface::TouchEvent& ev) {
			//canvasControl->setBackground(static_cast<ofxInterface::reddoGui::Coverflow*>(node->getChildWithName("gallery"))->getSelectedItem());
			//canvasControl->reloadBackground(true);

			// disable main elements in canvas


			for (auto& node : canvasControl->getCanvas()->getChildWithName("background")->getChildren())
			{
				node->deactivate();
			}
			for (auto& node : canvasControl->getCanvas()->getChildWithName("overlay")->getChildren())
			{
				node->deactivate();
			}

			canvasControl->getCanvas()->getChildWithName("overlay")->getChildWithName("node_logo_selfie")->deactivate();
			canvasControl->getCanvas()->getChildWithName("overlay")->getChildWithName("node_logo_team")->deactivate();
			canvasControl->getCanvas()->getChildWithName("overlay")->getChildWithName("node_logo_yogi")->deactivate();

			canvasControl->setBackgroundEnabled(true);
			canvasControl->setOverlayEnabled(true);
			canvasControl->getCanvas()->getChildWithName("overlay")->activate();

			
			// prepare scene
			canvasControl->setBackgroundEnabled(true);
			canvasControl->setOverlayEnabled(true);

			auto selectedScene = static_cast<ofxInterface::reddoGui::Coverflow*>(node->getChildWithName("gallery"))->getSelectedItem();
			ofJson canvasControlData = ofJson{ {"scene",""} };
			string nextState;

			

			switch (selectedScene)
			{
				//yogi
			case 0: {
				canvasControl->closeBackgroundVideo("node_background");
				canvasControl->closeOverlayVideo("node_overlay");
				canvasControl->getCanvas()->getChildWithName("background")->getChildWithName("yogi_bg")->activate();
				//canvasControl->getCanvas()->getChildWithName("overlay")->getChildWithName("node_overlay")->activate();
				canvasControl->loadOverlayVideo("node_overlay", toolSettings["scenes"]["yogi"]["overlayVideo"].get<string>());
				
				canvasControlData["scene"] = "yogi";
				canvasControl->setMetadata(canvasControlData);
				nextState = "lead";
				ofNotifyEvent(stateFinishedEvent, nextState);
				break;

			}
				//selfie
			case 1: {
				canvasControl->closeBackgroundVideo("node_background");
				canvasControl->closeOverlayVideo("node_overlay");
				canvasControl->getCanvas()->getChildWithName("background")->getChildWithName("selfie_bg")->activate();
				//canvasControl->getCanvas()->getChildWithName("background")->getChildWithName("node_background")->activate();
				canvasControl->loadBackgroundVideo("node_background", toolSettings["scenes"]["selfie"]["bgVideo"].get<string>());
				//canvasControl->getCanvas()->getChildWithName("overlay")->getChildWithName("node_overlay")->activate();
				canvasControl->loadOverlayVideo("node_overlay", toolSettings["scenes"]["selfie"]["overlayVideo"].get<string>());
				canvasControlData["scene"] = "selfie";
				canvasControl->setMetadata(canvasControlData);
				nextState = "lead";
				ofNotifyEvent(stateFinishedEvent, nextState);
				break;
			}
				//team
			case 2: {
				canvasControl->getCanvas()->getChildWithName("overlay")->getChildWithName("node_overlay")->deactivate();
				canvasControl->getCanvas()->getChildWithName("background")->getChildWithName("node_background")->deactivate();
				canvasControl->getCanvas()->getChildWithName("background")->getChildWithName("team_bg")->activate();
				canvasControlData["scene"] = "team";
				canvasControl->setMetadata(canvasControlData);
				canvasControl->closeBackgroundVideo("node_background");
				canvasControl->closeOverlayVideo("node_overlay");
				nextState = "selectPosition";
				ofNotifyEvent(stateFinishedEvent, nextState);
				break;
			}
			default:
				break;
			}

			

			
		}

		void SelectionState::activate() {
			ToolState::activate();
			static_cast<ofxInterface::reddoGui::Coverflow*>(node->getChildWithName("gallery"))->setSelectedItem(0);

			// show preview with overlay if only 1 available
			if (GPF::listFiles(toolSettings["config"]["overlay"]).size()) {
				canvasControl->setOverlayEnabled(true);
			}else {
				canvasControl->setOverlayEnabled(false);
			}
			
			for (size_t i = 0; i < previewFbos.size(); i++) {
				canvasControl->setBackground(i);
				canvasControl->drawInFbo(previewFbos[i]);
			}
		}

		void SelectionState::initState(PhotoToolStateConfig config) {
			vector<string> files;
			files.push_back(toolSettings["scenes"]["yogi"]["sample"].get<string>());
			files.push_back(toolSettings["scenes"]["selfie"]["sample"].get<string>());
			files.push_back(toolSettings["scenes"]["team"]["sample"].get<string>());

			for (auto& f:files) {
				ofTexture t = GPF::loadTexture(f);
				previews.push_back(t);

				ofxInterface::reddoGui::Coverflow* gallery = static_cast<ofxInterface::reddoGui::Coverflow*>(node->getChildWithName("gallery"));
				gallery->addTexture(previews.back());

				canvasControl->disableAllElements();
				ofAddListener(node->getChildWithName("next")->eventClick, this, &SelectionState::onStateEnd);
			}

			// add shadows
			ofxInterface::TextureNode* shadowTemplate = static_cast<ofxInterface::TextureNode*>(node->getChildWithName("shadowTemplate"));
			for (auto& pivot: node->getChildWithName("gallery")->getChildWithName("container")->getChildren())
			{
				ofxInterface::TextureNode* t = new ofxInterface::TextureNode();
				t->setTexture(shadowTemplate->getTexture());
				t->setPosition(shadowTemplate->getPosition());// -ofVec3f(pivot->getChildren()[0]->getSize()*0.5));
				t->setSize(shadowTemplate->getSize());
				t->setName("shadow");
				t->setPlane(-1);
				pivot->getChildren()[0]->addChild(t);
			}

			
		}
		void SelectionState::loadVideos()
		{
		
		}
	}
}
#pragma once
#include "PhotoToolState.h"
#include "SimpleTimer.h"
#include "ofxModulePictureExporter.h"
#include "ofxAnimatableFloat.h"

namespace reddo {
	namespace phototool {
		class WinState : public PhotoToolState
		{
		public:
			WinState();
			~WinState();

			void activate() override;
			void onStateEnd();
			void update() override;

			void logImage();

		protected:
			virtual void initState(PhotoToolStateConfig config);
			void proceedModuleEvent(ModuleEvent& e);

		private:

			ofVec2f pWrapperStart;
			ofVec2f pWrapperEnd;
			ofxAnimatableFloat wrapperAni;
			ofFile imageLog;
		};

	}
}
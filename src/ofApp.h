#pragma once
#define PRODUCT "CheilDfbPhototool"
#define VERSION "1.0.0"


#include "ofMain.h"
#include "ModuleControl.h"
#include "ToolControl.h"

#include "ofxModuleCanon.h"
#include "ofxModuleWebcam.h"
#include "Phototool.h"
#include "ofxModuleGreenscreen.h"
#include "ofxModulePictureExporter.h"
#include "ofxReddoPhototoolLighting.h"

using namespace ofxModule;

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		ModuleControl webModule;
		
};

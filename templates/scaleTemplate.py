import json
import os
import sys, getopt
import argparse

def updateElements(elements,factor):
	for e in elements:

		if 'size' in e:
			e['size'][0] *= factor
			e['size'][1] *= factor

		if 'position' in e:
			e['position'][0] *= factor
			e['position'][1] *= factor

		if 'posHead' in e:
			e['posHead'][0] *= factor
			e['posHead'][1] *= factor
		if 'sizeScrollableArea' in e:
			e['sizeScrollableArea'][0] *= factor
			e['sizeScrollableArea'][1] *= factor
		if 'scaleElements' in e:
			e['scaleElements'] *= factor
		if 'sizeHead' in e:
			e['sizeHead'][0] *= factor
			e['sizeHead'][1] *= factor
		if 'sizeRectangle' in e:
			e['sizeRectangle'][0] *= factor
			e['sizeRectangle'][1] *= factor
		if 'sizeThumb' in e:
			e['sizeThumb'][0] *= factor
			e['sizeThumb'][1] *= factor
		if 'sizeIcon' in e:
			e['sizeIcon'][0] *= factor
			e['sizeIcon'][1] *= factor
		if 'ringWidth' in e:
			e['ringWidth'] *= factor
		if 'widthCircle' in e:
			e['widthCircle'] *= factor
		if 'cornerRadius' in e:
			e['cornerRadius'] *= factor
		if 'shadowSize' in e:
			e['shadowSize']*= factor
		if 'borderRadius' in e:
			e['borderRadius']*= factor
		if 'borderWidth' in e:
			e['borderWidth']*= factor
		if 'thumbMargin' in e:
			e['thumbMargin']*= factor
		if 'paginationMargin' in e:
			e['paginationMargin']*= factor
		if 'labelMargin' in e:
			e['labelMargin']*= factor
		if 'dashAngleStroke' in e:
			e['dashAngleStroke']*= factor
		if 'dashAngleEmpty' in e:
			e['dashAngleEmpty']*= factor
		if 'labelHeight' in e:
			e['labelHeight']*= factor
		if 'arrowLineWidth' in e:
			e['arrowLineWidth']*= factor
		if 'arrowSize' in e:
			e['arrowSize'][0] *= factor
			e['arrowSize'][1] *= factor
		if 'lineWidth' in e:
			e['lineWidth']*= factor
		if 'arrowMargin' in e:
			e['arrowMargin'][0] *= factor
			e['arrowMargin'][1] *= factor
		if 'labelPosition' in e:
			e['labelPosition'][0] *= factor
			e['labelPosition'][1] *= factor
		if 'margin' in e:
			if type(e['margin']) is list:
				for id in range(len(e['margin'])):
					e['margin'][id] *= factor
			else:
				e['margin']*= factor
		if 'padding' in e:
			if type(e['padding']) is list:
				for id in range(len(e['padding'])):
					e['padding'][id] *= factor
			else:
				e['padding'] *= factor

		if 'elements' in e:
			updateElements(e['elements'],factor)
		if 'shadow' in e:
			if 'size' in e['shadow']:
				e['shadow']['size'] *= factor
			if 'y' in e['shadow']:
				e['shadow']['y'] *= factor	
			if 'x' in e['shadow']:
				e['shadow']['x'] *= factor	

def updateScale(src, dest, filename, factor):
	if not os.path.exists(dest):
		os.mkdir(dest)
		print("Directory " , dest ,  " Created ")
	else:    
		print("Directory " , dest ,  " already exists. Files will be overwritten.")

	with open(src + "/" + filename) as json_file:
		data = json.load(json_file)
		
		# font sizes
		if 'fontStyles' in data:
			for p in data['fontStyles']:
				p['fontSize'] *= factor
		json_file.close()

		# elements
		if 'elements' in data:
			updateElements(data['elements'],factor)
		
		if 'screens' in data:
			for p in data['screens']:
				updateElements(p['elements'],factor)
		
		if 'settings' in data:
			data['settings']['width'] *= factor
			data['settings']['height'] *= factor

	with open(dest + "/" + filename, 'w') as f:
		json.dump(data, f,indent=4,ensure_ascii=False)
		f.close()


scaleFactor = 2.0

parser = argparse.ArgumentParser(description='Scales the current gui to a factor (default 2.0)')
parser.add_argument('input_folder', type=str, help='the current configuration folder' )
parser.add_argument('output_folder', type=str, help='the output folder of the new configuration' )
parser.add_argument('--scale', type=float ,help='the scale factor. (FullHD -> 4K = 2.0)')

args = parser.parse_args()

folder = args.input_folder
newFolder = args.output_folder
if args.scale:
	scaleFactor = args.scale


updateScale(folder,newFolder,"guiElements.json",scaleFactor)
updateScale(folder,newFolder,"gui.json",scaleFactor)





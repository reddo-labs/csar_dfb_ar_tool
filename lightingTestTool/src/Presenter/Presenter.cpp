/*
 *  Queue.cpp
 *  printer
 *
 *  Created by Brian Eschrich on 09.10.15
 *  Copyright 2015 __MyCompanyName__. All rights reserved.
 *
 */

#include "Presenter.h"
#include "GPF.h"

namespace ofxModule {
    
	Presenter::Presenter(string moduleName, string settingsPath):ModuleDrawable("Presenter",moduleName,settingsPath){
		
		// register mouse events for interaction
		ofRegisterMouseEvents(this);

		// get paths for program config
		auto pathElems = ofSplitString(getSettingsPath(), "/");

		// kill the path after the .json
		int indexJson = -1;
		for (size_t i = 0; i < pathElems.size(); i++)
		{
			if (ofIsStringInString(pathElems[i], ".json")) {
				indexJson = i;
			}
		}
		if (indexJson != -1) {
			pathElems.erase(pathElems.begin() + indexJson, pathElems.end());
		}

		string basePath = "";
		if (pathElems.size() > 1) {
			for (size_t i = 0; i < pathElems.size(); i++) {
				basePath += pathElems[i] + "/";
			}
		}
		string guiDefinitionPath = basePath + "gui.json";

		ofJson gui = ofLoadJson(guiDefinitionPath);


		ofSetWindowShape(gui["settings"]["width"].get<int>(), gui["settings"]["height"].get<int>());
		ofSetWindowPosition(0, 0);

		ofSetBackgroundColor(255);
    }
  
    
    
    //------------------------------------------------------------------
    void Presenter::update() {
		// here could run something ;)
    }

	void Presenter::draw()
	{
		// draw the received text
		isLight ? ofSetColor(200) : ofSetColor(100);
		ofDrawRectangle(20, 20, 150, 50);
		isChroma ? ofSetColor(200) : ofSetColor(100);
		ofDrawRectangle(190, 20, 150, 50);

		ofSetColor(0);
		ofDrawBitmapString("Toggle Light", 30, 50);
		ofDrawBitmapString("Toggle Chroma", 200, 50);
	}

	void Presenter::mousePressed(ofMouseEventArgs & mouse)
	{
		if (mouse.y < 70) {
			if (mouse.x < 170) {
				if (!isLight) {
					notifyEvent("only_led_on");
				}
				else {
					notifyEvent("only_led_off");
				}
				isLight = !isLight;
			}
			else if (mouse.x < 340) {
				if (!isChroma) {
					notifyEvent("led_ring_on");
				}
				else {
					notifyEvent("led_ring_off");
				}
				isChroma = !isChroma;
			}
		}
		// prepare a message with the mouse position
		ofJson msg;

		// add a value
		msg["value"] = mouse.x;

		//send a message to Communicator
		notifyEvent("multiply",msg);
	}
    
    
    //------------------------------------------------------------------
    void Presenter::proceedModuleEvent(ModuleEvent& e) {
		

    }
    
}

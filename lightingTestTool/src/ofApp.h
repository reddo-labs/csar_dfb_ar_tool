#pragma once
#define PRODUCT "CheilDfbPhototool"
#define VERSION "1.0.0"

#include "ofMain.h"
#include "ofxReddoPhototoolLighting.h"
#include "ModuleControl.h"
#include "ToolControl.h"
#include "Presenter.h"

using namespace ofxModule;

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		ModuleControl webModule;
		
};

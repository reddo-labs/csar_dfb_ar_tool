import csv
from datetime import datetime
import collections

import pygal
from pygal.style import DarkStyle

renderStyle = DarkStyle
renderStyle.font_family = 'googlefont:Raleway'
renderStyle.value_label_font_size = 8


def place_photos(data):
    res = dict()
    for row in data:
        if row['place'] in res:
            res[row['place']] = res[row['place']] + 1
        else:
            res[row['place']] = 1
    return res


def date_place_photos(data):
    res = dict()
    for row in data:
        date = datetime.strptime(row['timestamp'], '%Y-%m-%d-%H-%M-%S-%f')
        dates = date.strftime('%a %m/%d')

        if dates in res:
            if row['place'] in res[dates]:
                res[dates][row['place']] += 1
            else:
                res[dates][row['place']] = 1
        else:
            res[dates] = {}
            res[dates][row['place']] = 1
    return res


def time_place_photos(data):
    res = dict()
    for row in data:
        date = datetime.strptime(row['timestamp'], '%Y-%m-%d-%H-%M-%S-%f')
        dates = date.strftime('%H:00 - %H:59')

        if dates in res:
            if row['place'] in res[dates]:
                res[dates][row['place']] += 1
            else:
                res[dates][row['place']] = 1
        else:
            res[dates] = {}
            res[dates][row['place']] = 1

    return collections.OrderedDict(sorted(res.items()))


def plot_place_photos(data):
    bar_chart = pygal.Bar(
        fill=True,
        style=renderStyle)

    bar_chart.title = "Fotos gesamt"
    bar_chart.x_labels = list(data.keys())
    bar_chart.add('Anzahl der Fotos', list(data.values()))
    bar_chart.render_to_file('all.svg')


def plot_date_place_photos(data):
    places = {}
    for date in data:
        for place in data[date]:
            if place in places:
                places[place].append(data[date][place])
            else:
                places[place] = []
                places[place].append(data[date][place])

    bar_chart = pygal.Bar(
        fill=True,
        style=renderStyle
    )
    bar_chart.title = "Fotos nach Datum"
    bar_chart.x_labels = list(data.keys())

    for place in places:
        bar_chart.add(place, places[place])
    bar_chart.render_to_file('date_place.svg')


def plot_time_place_photos(data):
    places = {}
    for date in data:
        for place in data[date]:
            if place in places:
                places[place].append(data[date][place])
            else:
                places[place] = []
                places[place].append(data[date][place])

    bar_chart = pygal.Bar(
        x_label_rotation=45,
        fill=True,
        style=renderStyle)
    bar_chart.title = "Fotos nach Uhrzeit"
    bar_chart.x_labels = list(data.keys())

    for place in places:
        bar_chart.add(place, places[place])
    bar_chart.render_to_file('date_time.svg')


# import index
index = {}
with open('data/index.csv', mode='r') as csv_file:
    index = csv.DictReader(csv_file)
    # load tables listes in index
    tables = []
    for row in index:
        with open('data/' + row['file'], mode='r') as value_file:
            values = csv.DictReader(value_file)
            # append place id and append to values
            for file_row in values:
                file_row['place'] = row['id']
                tables.append(dict(file_row))

    # plotting

    plot_place_photos(place_photos(tables))
    plot_date_place_photos(date_place_photos(tables))
    plot_time_place_photos(time_place_photos(tables))
